<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Farm extends Model
{

    public function zoning()
    {
        return $this->hasOne('App\Zoning','id','zoning_id');
    }

    public function farm_type()
    {
        return $this->hasOne('App\FarmType', 'id','farm_type_id');
    }

    public function farm_subject()
    {
        return $this->hasOne('App\FarmSubject', 'id','farm_subject_id');
    }
}
