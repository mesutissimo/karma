<?php

namespace App\Http\Controllers;

use App\Currency;
use App\DeedType;
use App\District;
use App\Farm;
use App\FarmSubject;
use App\FarmType;
use App\Flat;
use App\HeatingType;
use App\Land;
use App\Listing;
use App\ListingImage;
use App\ListingType;
use App\Presentation;
use App\Project;
use App\PropertyType;
use App\Province;
use App\Villa;
use App\Zoning;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ListingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $property_types = PropertyType::all();
        $listing_types = ListingType::all();
        // $currencies = Currency::all();
        $flats = Listing::with(['images', 'property_type', 'listing_type', 'province', 'district', 'currency', 'flat', 'farm', 'land'])->get();

        $listings = $flats;

        //$listings = Listing::with(['property_type', 'listing_type', 'province', 'district'])->get();

        // echo "<pre>"; print_r($listings);
        return view('listings.index',
            [
                'listings' => $listings,
                'property_types' => $property_types,
                'listing_types' => $listing_types,
            ]);

    }

    public function create(Request $request)
    {
        $unique_id = uniqid();
        $provinces = Province::all(); // Getting all provinces
        $deed_types = DeedType::all(); // Getting all provinces
        $districts = District::where('province_id', 40)->get(); // Checking Istanbul Districts
        $property_type = PropertyType::find($request->property_type);
        $listing_type = ListingType::find($request->listing_type);
        $currencies = Currency::all();
        $heating_types = HeatingType::all();
        $zonings = Zoning::all();
        $farm_types = FarmType::all();
        $farm_subjects = FarmSubject::all();


        return view(
            'listings.create',
            [
                'edit' => false,
                'unique_id' => $unique_id,
                'provinces' => $provinces,
                'districts' => $districts,
                'currencies' => $currencies,
                'deed_types' => $deed_types,
                'zonings' => $zonings,
                'farm_types' => $farm_types,
                'farm_subjects' => $farm_subjects,
                'heating_types' => $heating_types,
                'property_type' => $property_type,
                'listing_type' => $listing_type,
            ]);

    }

    public function store(Request $request)

    {
        $this->validate($request, [
            'title' => 'required',
            'presentation' => 'mimes:pdf|nullable|max:10000'
        ]);

        // Handle Presentation Upload

        if ($request->hasFile('presentation')) {
            $path = $request->file('presentation')->store('presentations');
        }

        $listing = new Listing;
        $listing->unique_id = $request->unique_id;
        $listing->title = $request->title;
        $listing->property_type_id = $request->property_type;
        $listing->listing_type_id = $request->listing_type;
        $listing->province_id = $request->province;
        $listing->district_id = $request->district;
        $listing->price = $request->price;
        $listing->currency_id = $request->currency;
        $listing->details = $request->details;
        $listing->precedent_value = $request->precedent_value;


        if ($request->hasFile('presentation')) {
            $listing->presentation = $request->file('presentation')->hashName();
        }
        $listing->save();

        switch ($request->property_type) {
            case 1:

                $estate = new Flat;
                $estate->unique_id = $request->unique_id;
                $estate->gross_area = $request->gross_area;
                $estate->net_area = $request->net_area;
                $estate->room = $request->room;
                $estate->saloon = $request->saloon;
                $estate->bath = $request->bath;
                $estate->building_age = $request->building_age;
                $estate->flats_floor = $request->flats_floor;
                $estate->building_floor_count = $request->building_floor_count;
                $estate->balcony = ($request->balcony) ? 1 : 0;
                $estate->furnished = ($request->furnished) ? 1 : 0;
                $estate->heating_type_id = $request->heating_type;
                $estate->deed_type_id = $request->deed_type;
                $estate->save();

                break;

            case 2:

                $land = new Land;
                $land->unique_id = $request->unique_id;
                $land->rff = 0;
                $land->gross_area = $request->gross_area;
                $land->net_area = $request->net_area;
                $land->block = $request->block;
                $land->plot = $request->plot;
                $land->zoning_id = $request->zoning;
                $land->save();

                break;

            case 3:

                $land = new Land;
                $land->unique_id = $request->unique_id;
                $land->rff = 1;
                $land->gross_area = $request->gross_area;
                $land->net_area = $request->net_area;
                $land->block = $request->block;
                $land->plot = $request->plot;
                $land->zoning_id = $request->zoning;
                $land->total_construction_area = $request->total_construction_area;
                $land->rff_rate = $request->rff_rate;
                $land->apprx_cost = $request->apprx_cost;
                $land->apprx_gain = $request->apprx_gain;
                $land->apprx_timetofinish = $request->apprx_timetofinish;
                $land->kaks = $request->kaks;
                $land->taks = $request->taks;

                $land->save();

                break;

            case 4:
                // TODO: Change variable name to a generic type

                $farm = new Farm;
                $farm->unique_id = $request->unique_id;
                $farm->net_area = $request->net_area;
                $farm->closed_area = $request->closed_area;
                $farm->block = $request->block;
                $farm->plot = $request->plot;
                $farm->farm_type_id = $request->farm_type;
                $farm->farm_subject_id = $request->farm_subject;
                $farm->is_active = ($request->is_active) ? 1 : 0;
                $farm->animal_count = $request->animal_count;
                $farm->save();

                break;

            case 5:

                $estate = new Villa;
                $estate->unique_id = $request->unique_id;
                $estate->gross_area = $request->gross_area;
                $estate->net_area = $request->net_area;
                $estate->room = $request->room;
                $estate->saloon = $request->saloon;
                $estate->bath = $request->bath;
                $estate->building_age = $request->building_age;
                $estate->floor_count = $request->floor_count;
                $estate->balcony = ($request->balcony) ? 1 : 0;
                $estate->furnished = ($request->furnished) ? 1 : 0;
                $estate->security = $request->security;
                $estate->swimming_pool_id = $request->swimming_pool;
                $estate->playground = $request->playground;
                $estate->sportsarea = $request->sportsarea;
                $estate->walkingpath = $request->walkingpath;
                $estate->parking = $request->parking;
                $estate->heating_type_id = $request->heating_type;
                $estate->deed_type_id = $request->deed_type;
                $estate->save();

                break;

            case 6:

                $estate = new Project;
                $estate->unique_id = $request->unique_id;
                $estate->gross_area = $request->gross_area;
                $estate->net_area = $request->net_area;
                $estate->room = $request->room;
                $estate->saloon = $request->saloon;
                $estate->bath = $request->bath;
                $estate->building_age = $request->building_age;
                $estate->floor_count = $request->floor_count;
                $estate->balcony = ($request->balcony) ? 1 : 0;
                $estate->furnished = ($request->furnished) ? 1 : 0;
                $estate->security = $request->security;
                $estate->swimming_pool_id = $request->swimming_pool;
                $estate->playground = $request->playground;
                $estate->sportsarea = $request->sportsarea;
                $estate->walkingpath = $request->walkingpath;
                $estate->parking = $request->parking;
                $estate->heating_type_id = $request->heating_type;
                $estate->deed_type_id = $request->deed_type;
                $estate->save();

                break;

        }

        return redirect('listings');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $listing = Listing::where('id', $id)->get();
        $provinces = Province::all(); // Getting all provinces
        $districts = District::all(); // Getting all districts
        $deed_types = DeedType::all();
        $currencies = Currency::all();
        $heating_types = HeatingType::all();
        $zonings = Zoning::all();
        $farm_types = FarmType::all();
        $farm_subjects = FarmSubject::all();


        return view(
            'listings.edit',
            [
                'edit' => true,
                'listing' => $listing[0],
                'provinces' => $provinces,
                'districts' => $districts,
                'currencies' => $currencies,
                'heating_types' => $heating_types,
                'zonings' => $zonings,
                'deed_types' => $deed_types,
                'farm_types' => $farm_types, 'farm_subjects' => $farm_subjects
            ]);
    }


    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'presentation' => 'mimes:pdf|nullable|max:10000'

        ]);

        if ($request->hasFile('presentation')) {
            $path = $request->file('presentation')->store('presentations');
        }


        $listing = Listing::find($id);
        $listing->unique_id = $request->unique_id;
        $listing->title = $request->title;
        $listing->property_type_id = $request->property_type;
        $listing->listing_type_id = $request->listing_type;
        $listing->province_id = $request->province;
        $listing->district_id = $request->district;
        $listing->price = $request->price;
        $listing->currency_id = $request->currency;
        $listing->details = $request->details;
        $listing->precedent_value = $request->precedent_value;

        if ($request->hasFile('presentation')) {
            $listing->presentation = $request->file('presentation')->hashName();
        }

        $listing->save();

        switch ($listing->property_type_id) {
            case 1:

                $estate = Flat::where('unique_id', $listing->unique_id)->firstOrFail();
                $estate->unique_id = $request->unique_id;
                $estate->gross_area = $request->gross_area;
                $estate->net_area = $request->net_area;
                $estate->room = $request->room;
                $estate->saloon = $request->saloon;
                $estate->bath = $request->bath;
                $estate->building_age = $request->building_age;
                $estate->flats_floor = $request->flats_floor;
                $estate->building_floor_count = $request->building_floor_count;
                $estate->balcony = ($request->balcony) ? 1 : 0;
                $estate->furnished = ($request->furnished) ? 1 : 0;
                $estate->heating_type_id = $request->heating_type;
                $estate->deed_type_id = $request->deed_type;
                $estate->save();

                break;

            case 2:

                $land = Land::where('unique_id', $listing->unique_id)->firstOrFail();
                $land->unique_id = $request->unique_id;
                $land->rff = 0;
                $land->gross_area = $request->gross_area;
                $land->net_area = $request->net_area;
                $land->block = $request->block;
                $land->plot = $request->plot;
                $land->zoning_id = $request->zoning;

                $land->save();

                break;

            case 3:

                $land = Land::where('unique_id', $listing->unique_id)->firstOrFail();
                $land->unique_id = $request->unique_id;
                $land->rff = 1;
                $land->gross_area = $request->gross_area;
                $land->net_area = $request->net_area;
                $land->block = $request->block;
                $land->plot = $request->plot;
                $land->zoning_id = $request->zoning;
                $land->total_construciton_area = $request->total_construciton_area;
                $land->rff_rate = $request->rff_rate;
                $land->apprx_cost = $request->apprx_cost;
                $land->apprx_gain = $request->apprx_gain;
                $land->apprx_timetofinish = $request->apprx_timetofinish;
                $land->kaks = $request->kaks;
                $land->taks = $request->taks;

                $land->save();

                break;

            case 4:

                $farm = Farm::where('unique_id', $listing->unique_id)->firstOrFail();
                $farm->unique_id = $request->unique_id;
                $farm->net_area = $request->net_area;
                $farm->closed_area = $request->closed_area;
                $farm->block = $request->block;
                $farm->plot = $request->plot;
                $farm->farm_type_id = $request->farm_type;
                $farm->farm_subject_id = $request->farm_subject;
                $farm->is_active = ($request->is_active) ? 1 : 0;
                $farm->animal_count = $request->animal_count;

                $farm->save();

                break;

            case 5:

                $estate = Villa::where('unique_id', $listing->unique_id)->firstOrFail();
                $estate->unique_id = $request->unique_id;
                $estate->gross_area = $request->gross_area;
                $estate->net_area = $request->net_area;
                $estate->room = $request->room;
                $estate->saloon = $request->saloon;
                $estate->bath = $request->bath;
                $estate->building_age = $request->building_age;
                $estate->villa_floor_count = $request->villa_floor_count;
                $estate->balcony = ($request->balcony) ? 1 : 0;
                $estate->furnished = ($request->furnished) ? 1 : 0;
                $estate->heating_type_id = $request->heating_type;
                $estate->deed_type_id = $request->deed_type;
                $estate->save();

                $estate->save();

                break;

            case 6:

                $estate = Project::where('unique_id', $listing->unique_id)->firstOrFail();
                $estate->unique_id = $request->unique_id;
                $estate->gross_area = $request->gross_area;
                $estate->net_area = $request->net_area;
                $estate->room = $request->room;
                $estate->saloon = $request->saloon;
                $estate->bath = $request->bath;
                $estate->building_age = $request->building_age;
                $estate->floor_count = $request->floor_count;
                $estate->balcony = ($request->balcony) ? 1 : 0;
                $estate->furnished = ($request->furnished) ? 1 : 0;
                $estate->security = $request->security;
                $estate->swimming_pool = $request->swimming_pool;
                $estate->playground = $request->playground;
                $estate->sportsarea = $request->sportsarea;
                $estate->walkingpath = $request->walkingpath;
                $estate->parking = $request->parking;
                $estate->heating_type_id = $request->heating_type;
                $estate->deed_type_id = $request->deed_type;
                $estate->save();


        }

        return redirect('listings');

    }


    public function destroy($id)

    {
        $listing = Listing::find($id);
        $listing->delete();

        $images = ListingImage::where('listing_uid', $listing->unique_id);
        $images->delete();

        return redirect('listings');

    }

    public function download_presentation($uid)
    {
        $listing = Listing::where('unique_id', $uid)->get();
        $filename = $listing[0]->presentation;
        $downloadname = 'p' . $uid . '_' . $listing[0]->title;
        return Storage::download('presentations/' . $filename, $downloadname . '.pdf');
    }

    public function delete_presentation($uid)
    {
        $listing = Listing::where('unique_id', $uid)->firstOrFail();
        $listing->update(['presentation' => null]);
        return 'Presentation Deleted';
    }


}
