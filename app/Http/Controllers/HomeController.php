<?php

namespace App\Http\Controllers;

use App\Listing;
use App\PropertyType;
use App\Settings\SliderImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public function index()
    {
        $slide_images = SliderImage::all();
        $property_types = PropertyType::all();

        $listings = Listing::with(['images','property_type', 'listing_type', 'province', 'district','heating_type'])->limit(15)->get();
        return view('front.home',[
            'listings' => $listings,
            'slider_images' => $slide_images,
            'recent_listings' => $listings,
            'property_types'=> $property_types
        ]);



    }

}
