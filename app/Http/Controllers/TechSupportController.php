<?php

namespace App\Http\Controllers;

use App\TechSupportIssue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TechSupportController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }

    public function index()
    {
        $issues = TechSupportIssue::where('owner' , Auth::user()->id)->orderBy('created_at','DESC')->get();

//        echo "<pre>";print_r($issues); die();
        return view('techsupport.index' , ['issues' => $issues]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('techsupport.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'subject' => 'required',
            'detail' => 'required'
        ]);

        $issue = new TechSupportIssue();
        $issue->owner = Auth::user()->id;
        $issue->subject = $request->subject;
        $issue->detail = $request->detail;
        $issue->save();

        return redirect(route('techsupport.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
