<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class AdminHomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {

        $listings = Listing::with(['images', 'property_type', 'listing_type', 'province', 'district', 'currency', 'flat', 'farm', 'land'])->orderBy('watch_count','DESC')->get();
        session(['applocale' => 'tr']);
        return view('adminhome', ['listings' => $listings]);
    }
}
