<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Http\Request;

class LandController extends Controller

{
    public function index() {
        $listings = Listing::limit(15)->get();
        $lands = Listing::where('property_type_id', 2)->limit(15)->get();
        return view('front.lands',['estates' => $lands , 'recent_listings' => $listings]);
    }
}
