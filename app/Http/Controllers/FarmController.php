<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Http\Request;

class FarmController extends Controller
{
    public function index() {
        $listings = Listing::limit(15)->get();
        $farms = Listing::where('property_type_id', 4)->limit(15)->get();
        return view('front.farms',['estates' => $farms, 'recent_listings' => $listings]);
    }
}
