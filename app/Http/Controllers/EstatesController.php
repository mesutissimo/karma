<?php

namespace App\Http\Controllers;

use App\Listing;
use App\PropertyType;
use Illuminate\Http\Request;

class EstatesController extends Controller
{
    public function index($pt) {

        $all_listings = Listing::limit(15)->get();

        $property_type = PropertyType::where('identifier', mb_strtolower($pt))->firstOrFail();

        $listings = Listing::where('property_type_id', $property_type->id)->limit(15)->get();

        return view('front.estates',[
            'type' => $property_type->identifier,
            'estates' => $listings,
            'recent_listings' => $all_listings
        ]);


    }
}
