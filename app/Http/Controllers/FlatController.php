<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Http\Request;

class FlatController extends Controller
{
    public function index() {

        $listings = Listing::limit(15)->get();
        $flats = Listing::where('property_type_id', 1)->limit(15)->get();
        return view('front.flats',['estates' => $flats , 'recent_listings' => $listings]);

    }
}
