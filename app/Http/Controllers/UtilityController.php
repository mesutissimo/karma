<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UtilityController extends Controller
{
    //
    public function getDistricts($id) {
        $districts = DB::table("districts")->where("province_id",$id)->pluck("name","id");
        return json_encode($districts);
    }

    public function getProvinces() {
        $provinces = DB::table("provinces")->get()->pluck("name","id");
        return json_encode($provinces);
    }
}
