<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Http\Request;

class DetailsController extends Controller
{
    public function index($listing_uid)
    {

        $listing = Listing::where('unique_id', $listing_uid)->first();

        if (isset($listing)) {

           // echo "<pre>"; print_r($listing);die();
            $listing->watch_count = $listing->watch_count + (1);
            $listing->save();

            return view('front.listing_detail', ['listing' => $listing ]);

        } else {
            abort(404);
        }


    }
}
