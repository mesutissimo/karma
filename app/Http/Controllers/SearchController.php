<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {


        if ($request->has('search_query') && $request->search_query != '') {
            $estates = Listing::with(['province', 'district'])
                ->where([
                        ['title', 'LIKE', '%' . $request->search_query . '%'],
                        ['property_type_id', $request->property_type]
                    ]
                )
                ->orWhereHas('province', function ($q) use ($request) {
                    $q->where('name', 'like', '%' . $request->search_query . '%');
                })
                ->orWhereHas('district', function ($q) use ($request) {
                    $q->where('name', 'like', '%' . $request->search_query . '%');
                })
                ->get();

            $estates = $estates->where('property_type_id', '=', $request->property_type);

            if (count($estates) > 0) {
                return view('front.estates',
                    [
                        'listings' => $estates,
                        'recent_listings' => Listing::limit(15)->get()
                    ]);
            } else {
                return redirect('/');
            }


        } else {
            return redirect('/');
        }

    }
}
