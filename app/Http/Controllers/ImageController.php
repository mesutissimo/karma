<?php

namespace App\Http\Controllers;

use App\ListingImage;
use App\Settings\SliderImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Intervention\Image\ImageManagerStatic as Image;

class ImageController extends Controller

{
    public function __construct()
    {
        //Photo Paths

        $this->photos_path = public_path('/images');
        $this->slider_images = public_path('/img/slider');
    }

    public function upload(Request $request)
    {

        $photos = $request->file('file');

        if (!is_array($photos)) {
            $photos = [$photos];
        }

        switch ($request->target) {

            case 'listing':


                if (!is_dir($this->photos_path)) {
                    mkdir($this->photos_path, 0777);
                }

                for ($i = 0; $i < count($photos); $i++) {
                    $photo = $photos[$i];
                    $name = sha1(date('YmdHis') . $request->listing_uid);
                    $save_name = $name . '.' . $photo->getClientOriginalExtension();
                    $resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

                    Image::make($photo)
                        ->resize(null, 600, function ($constraints) {
                            $constraints->aspectRatio();
                        })
                        ->save($this->photos_path . '/' . $resize_name);

                    $photo->move($this->photos_path, $save_name);

                    $upload = new ListingImage();
                    $upload->listing_uid = $request->listing_uid;
                    $upload->filename = $save_name;
                    $upload->resized_name = $resize_name;
                    $upload->original_name = basename($photo->getClientOriginalName());
                    $upload->save();
                }
                return Response::json([
                    'message' => 'Listing Image Saved Successfully ..!'
                ], 200);

                break;

            case 'fp_slider':

                if (!is_dir($this->slider_images)) {
                    mkdir($this->slider_images, 0777);
                }

                for ($i = 0; $i < count($photos); $i++) {
                    $photo = $photos[$i];
                    $name = sha1(date($i . 'YmdHis'));
                    $save_name = $name . '.' . $photo->getClientOriginalExtension();
                    $resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

                    Image::make($photo)
//                        ->resize(null, null, function ($constraints) {
//                            $constraints->aspectRatio();
//                        })
                        ->save($this->slider_images . '/' . $resize_name);

                    $photo->move($this->slider_images, $save_name);

                    $upload = new SliderImage();
                    $upload->filename = $resize_name;
                    $upload->save();
                }
                return Response::json([
                    'message' => 'Slider Image Saved Successfully ..!'
                ], 200);


                break;

            default:
        }

    }

    public function get_images(Request $request)
    {


        switch ($request->target) {

            case 'listing':

                $listing_uid = $request->uid;

                $image_urls = ListingImage::where('listing_uid', $listing_uid)->get();
                return response()->json($image_urls);

                break;

            case 'fp_slider':

                $image_urls = SliderImage::all();
                return response()->json($image_urls);

                break;

            default:
        }


    }

    public function delete_image(Request $request)
    {

        $image_to_delete = $request->removed_image;
        $encodednames = utf8_encode($image_to_delete); // Don't forget the encoding
        $data = json_decode($encodednames);
        $namesToDelete = [];
        foreach ($data as $d) {
            array_push($namesToDelete, $d->name);
        }

        switch ($request->target) {
            case 'listing':
                $deleted_image = ListingImage::whereIn('resized_name', $namesToDelete)->delete();
                break;
            case 'fp_slider':
                $deleted_image = SliderImage::whereIn('filename', $namesToDelete)->delete();
                break;
        }

        return response()->json(['msg' => 'ok']);


    }
}