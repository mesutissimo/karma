<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Http\Request;

class VillaController extends Controller
{

    public function index()
    {
        $listings = Listing::limit(15)->get();
        $villas = Listing::where('property_type_id', 5)->limit(15)->get();
        return view('front.villas',['estates' => $villas , 'recent_listings' => $listings]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
