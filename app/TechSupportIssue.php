<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechSupportIssue extends Model
{
    public function status() {

        return $this->hasOne('App\IssueStatus' , 'id','status_id');
    }
}
