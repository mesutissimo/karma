<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{

    protected $fillable = ['presentation'];

    public function images() {
        return $this->hasMany('App\ListingImage' , 'listing_uid','unique_id');
    }

    public function property_type()
    {
        return $this->belongsTo('App\PropertyType');
    }

    public function listing_type()
    {
        return $this->hasOne('App\ListingType' , 'id', 'listing_type_id');
    }

    public function province()
    {
        return $this->belongsTo('App\Province');
    }

    public function district()
    {
        return $this->belongsTo('App\District');
    }

    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }

    public function heating_type()
    {
        return $this->belongsTo('App\HeatingType');
    }

    public function deed_type()
    {
        return $this->belongsTo('App\DeedType');
    }

    public function swimming_pool()
    {
        return $this->belongsTo('App\SwimmingPool');
    }
    public function zoning()
    {
        return $this->belongsTo('App\Zoning');
    }

    public function flat()
    {
        return $this->belongsTo('App\Flat', 'unique_id', 'unique_id');
    }

    public function land()
    {
        return $this->belongsTo('App\Land','unique_id', 'unique_id');
    }

    public function farm()
    {
        return $this->belongsTo('App\Farm','unique_id', 'unique_id');
    }

    public function villa()
    {
        return $this->belongsTo('App\Villa','unique_id', 'unique_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Project','unique_id', 'unique_id');
    }

    public function farm_type()
    {
        return $this->belongsTo('App\FarmType');
    }

    public function farm_subject()
    {
        return $this->belongsTo('App\FarmSubject');
    }

}
