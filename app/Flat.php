<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flat extends Model
{

    public function province()
    {
        return $this->belongsTo('App\Province');
    }

    public function district()
    {
        return $this->belongsTo('App\District');
    }

    public function heating_type()
    {
        return $this->hasOne('App\HeatingType', 'id','heating_type_id');
    }

    public function deed_type()
    {
        return $this->belongsTo('App\DeedType');
    }
}
