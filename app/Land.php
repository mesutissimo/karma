<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Land extends Model
{


    public function zoning()
    {
        return $this->hasOne('App\Zoning','id','zoning_id');
    }

}
