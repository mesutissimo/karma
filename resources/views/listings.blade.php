@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">İlkuytvrıurvıufryvıkuyfvan Listesi</div>

                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Vitrin Fotoğrafı</th>
                                <th>İlan Başlığı</th>
                                <th>İlan Tipi</th>
                                <th>Konum</th>
                                <th>Fiyat</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($listings as $listing)
                                <tr>
                                    <td>
                                        <input type="checkbox" value="{{ $listing->id }} ">
                                    </td>
                                    <td>
                                        <img class="img img-responsive img-circle" src="img/dummy_home.png" alt="">
                                    </td>
                                    <td>
                                        {{ $listing->title  }}
                                    </td>
                                    <td>
                                        {{ $listing->type  }}
                                    </td>
                                    <td>
                                        {{ $listing->location  }}
                                    </td>
                                    <td>
                                        {{ $listing->price  }}
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('listings/edit') }}" class="btn btn-xs btn-primary">
                                                <i class="far fa-edit"></i>
                                            </a>
                                            <a class="btn btn-xs btn-danger">
                                                <i class="far fa-trash-alt "></i>
                                            </a>
                                        </div>

                                    </td>

                                </tr>

                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
