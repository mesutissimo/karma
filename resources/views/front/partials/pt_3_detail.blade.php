<tr>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Brüt Alan :</td>
    <td>{{ $listing->land->gross_area }} m<sup>2</sup></td>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Net Alan :</td>
    <td>{{ $listing->land->net_area }} m<sup>2</sup></td>
</tr>
<tr>
    <td><img class="micon" src="/svg/032-plan.svg" title=""/> Ada / Parsel:</td>
    <td>{{ $listing->land->block }} / {{ $listing->land->plot }}</td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td><img class="micon" src="/svg/014-crane.svg" title=""/> İmar Durumu :</td>
    <td colspan="3">{{ $listing->land->zoning->name }}</td>

</tr>
<tr>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Toplam İnşaat Alanı :</td>
    <td colspan="3" class="text-left">{{ $listing->land->total_construction_area }} m<sup>2</sup></td>
</tr>
<tr>
    <td><img class="micon" src="/svg/006-real-estate.svg" title=""/> Kat Karşılığı Oranı :</td>
    <td>% {{ $listing->land->rff_rate }}</td><td></td><td></td>
</tr>
<tr>
    <td><img class="micon" src="/svg/006-real-estate.svg" title=""/> Yaklaşık Maliyet :</td>
    <td>{{ $listing->land->apprx_cost }} {{ $listing->currency->symbol }}</td>
    <td><img class="micon" src="/svg/006-real-estate.svg" title=""/> Yaklaşık Kazanç :</td>
    <td>{{ $listing->land->apprx_gain }} {{ $listing->currency->symbol }}</td>
</tr>