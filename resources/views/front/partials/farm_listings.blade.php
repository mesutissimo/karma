<section class="probootstrap-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 section-heading probootstrap-animate">
            </div>
        </div>
        <div class="row mb70">

            @if(count($estates) > 0)

            @foreach($estates as $estate)

                <div class="col-md-4 probootstrap-animate">

                    <div class="probootstrap-block-image">
                        <figure>
                            <a class="im" href="details/{{$estate->unique_id}}">
                                @foreach($estate->images as $image)
                                    <img src="images/{{ $image->resized_name }}" alt="{{ $estate->title }}" style="height: 250px">
                                @endforeach
                            </a>
                        </figure>
                        <div class="text">
                            <h3><a href="#">{{ $estate->title }}</a></h3>
                            <h4>
                                <a href="#"><i class="fas fa-map-marker-alt"></i> {{ $estate->province->name }}</a><br>
                                <small>
                                    <a href="#">{{ $estate->district->name }}</a>
                                </small>
                            </h4>

                            <h2>
                                <strong>
                                    </i>{{ number_format($estate->price,0,',','.') }} {{ $estate->currency->symbol }}
                                </strong>
                            </h2>

                            {{-- Specific Details --}}

                            @switch ($estate->property_type->id)
                                @case(1)
                                <p class="dark">{{ $estate->property_type->name }}
                                    @if( $estate->farm->furnished )
                                        <small> ( <i class="fas fa-couch"></i> Eşyalı )</small>
                                    @endif
                                </p>
                                <p class="dark">
                                    <img class="micon" src="/svg/034-area.svg" title=""/>
                                    {{ $estate->farm->net_area }} m<sup>2</sup>
                                </p>


                                <p class="dark">
                                    <img class="micon" src="/svg/032-plan.svg" title="Oda Sayısı"/>
                                    {{ $estate->farm->room }}+{{ $estate->farm->saloon }}
                                </p>

                                @break
                                @case(2)
                                <p class="dark">{{ $estate->property_type->name }}</p>
                                <p class="dark">
                                    <img class="micon" src="/svg/034-area.svg" title=""/>
                                    <strong>Net</strong> {{ $estate->farm->net_area }} m<sup>2</sup>
                                </p>


                                <p class="dark">
                                    <img class="micon" src="/svg/deed.svg" title="İmar Durumu"/>
                                    {{ $estate->farm->zoning->name }}
                                </p>
                                @break
                                @case(3)
                                <p class="dark">{{ $estate->property_type->name }}</p>
                                <p class="dark">
                                    <img class="micon" src="/svg/034-area.svg" title=""/>
                                    <strong>Net</strong> {{ $estate->farm->net_area }} m<sup>2</sup>
                                </p>


                                <p class="dark">
                                    <img class="micon" src="/svg/deed.svg" title="İmar Durumu"/>
                                    {{ $estate->farm->zoning->name }}
                                </p>
                            @break
                                @case(4) {{--  Farm Type  --}}
                                <p class="dark">{{ $estate->property_type->name }}</p>
                                <p class="dark">
                                    <img class="micon" src="/svg/034-area.svg" title=""/>
                                    <strong>Net</strong> {{ $estate->farm->net_area }} m<sup>2</sup>
                                </p>
                                <p class="dark">
                                    @switch($estate->farm->farm_type->id)
                                        @case(1)
                                        <img class="micon" src="/svg/cow.svg" title="Büyükbaş"/>
                                        @break
                                        @case(2)
                                        <img class="micon" src="/svg/sheep.svg" title="Küçükbaş"/>
                                        @break
                                        @default
                                    @endswitch
                                       x  {{ $estate->farm->animal_count }}
                                </p>

                                @default
                            @endswitch
                            {{--<p class="secondary-color rate">
                                <i class="icon-star-full"></i>
                                <i class="icon-star-full"></i>
                                <i class="icon-star-full"></i>
                                <i class="icon-star-full"></i>
                                <i class="icon-star-half"></i>
                            </p>
                            --}}
                            <hr>
                            <p class="clearfix like">
                                <a class="pull-left" href="#">
                                    @if($estate->watch_count > 0)
                                        <i class="icon-eye"></i>
                                        {{ $estate->watch_count }} defa incelendi .
                                    @else
                                        <i class="icon-new"></i>

                                        Yeni ilan
                                    @endif
                                </a>
                            <a href="details/{{$estate->unique_id}}" class="btn btn-primary btn-sm link-with-icon pull-right">İncele<i class=" icon-chevron-right"></i></a>
                            </p>
                        </div>
                    </div>

                </div>

            @endforeach



                @else
               Gayrimenkul bulunamadı
            @endif
        </div>
    </div>
</section>