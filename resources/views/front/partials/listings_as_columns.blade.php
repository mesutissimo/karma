@foreach($listings as $listing)

    <div class="col-md-4 probootstrap-animate">

        <div class="probootstrap-block-image">
            <figure>
                <a class="im" href="details/{{$listing->unique_id}}">
                    @foreach($listing->images as $image)
                        <img src="images/{{ $image->resized_name }}" alt="{{ $listing->title }}" style="height: 250px">
                    @endforeach
                </a>
            </figure>
            <div class="text">
                <h3><a href="#">{{ $listing->title }}</a></h3>
                <h4>
                    <a href="#"><i class="fas fa-map-marker-alt"></i> {{ $listing->province->name }}</a><br>
                    <small>
                        <a href="#">{{ $listing->district->name }}</a>
                    </small>
                </h4>

                <h2>
                    <strong>
                        </i>{{ number_format($listing->price,0,',','.') }} {{ $listing->currency->symbol }}
                    </strong>
                </h2>

                {{-- Specific Details --}}

                @switch ($listing->property_type->id)
                    @case(1)
                    <p class="dark">{{ $listing->property_type->name }}
                        @if( $listing->flat->furnished )
                            <small> ( <i class="fas fa-couch"></i> Eşyalı )</small>
                        @endif
                    </p>
                    <p class="dark">
                        <img class="micon" src="/svg/034-area.svg" title=""/>
                        {{ $listing->flat->net_area }} m<sup>2</sup>
                    </p>


                    <p class="dark">
                        <img class="micon" src="/svg/032-plan.svg" title="Oda Sayısı"/>
                        {{ $listing->flat->room }}+{{ $listing->flat->saloon }}
                    </p>

                    @break
                    @case(2)
                    <p class="dark">{{ $listing->property_type->name }}</p>
                    <p class="dark">
                        <img class="micon" src="/svg/034-area.svg" title=""/>
                        {{ $listing->land->net_area }} m<sup>2</sup>
                    </p>


                    <p class="dark">
                        <img class="micon" src="/svg/deed.svg" title="İmar Durumu"/>
                        {{ $listing->land->zoning->name }}
                    </p>
                    @break
                    @case(3)
                    <p class="dark">{{ $listing->property_type->name }}</p>
                    <p class="dark">
                        <img class="micon" src="/svg/034-area.svg" title=""/>
                        {{ $listing->land->net_area }} m<sup>2</sup>
                    </p>


                    <p class="dark">
                        <img class="micon" src="/svg/deed.svg" title="İmar Durumu"/>
                        {{ $listing->land->zoning->name }}
                    </p>
                    @break
                    @case(4) {{--  Farm Type  --}}
                    <p class="dark">{{ $listing->property_type->name }}</p>
                    <p class="dark">
                        <img class="micon" src="/svg/034-area.svg" title=""/>
                        {{ $listing->farm->net_area }} m<sup>2</sup>
                    </p>
                    <p class="dark">
                        @switch($listing->farm->farm_type->id)
                            @case(1)
                            <img class="micon" src="/svg/cow.svg" title="Büyükbaş"/>
                            @break
                            @case(2)
                            <img class="micon" src="/svg/sheep.svg" title="Küçükbaş"/>
                            @break
                            @default
                        @endswitch
                        x  {{ $listing->farm->animal_count }}
                    </p>
                    @case(5)
                    <p class="dark">{{ $listing->property_type->name }}
                        @if( $listing->villa->furnished )
                            <small> ( <i class="fas fa-couch"></i> Eşyalı )</small>
                        @endif
                    </p>
                    <p class="dark">
                        <img class="micon" src="/svg/034-area.svg" title=""/>
                        {{ $listing->villa->net_area }} m<sup>2</sup>
                    </p>


                    <p class="dark">
                        <img class="micon" src="/svg/032-plan.svg" title="Oda Sayısı"/>
                        {{ $listing->villa->room }}+{{ $listing->villa->saloon }}
                    </p>

                    @break
                    @case(6)
                    <p class="dark">{{ $listing->property_type->name }}
                        @if( $listing->project->furnished )
                            <small> ( <i class="fas fa-couch"></i> Eşyalı )</small>
                        @endif
                    </p>
                    <p class="dark">
                        <img class="micon" src="/svg/034-area.svg" title=""/>
                        {{ $listing->project->net_area }} m<sup>2</sup>
                    </p>


                    <p class="dark">
                        <img class="micon" src="/svg/032-plan.svg" title="Oda Sayısı"/>
                        {{ $listing->project->room }}+{{ $listing->project->saloon }}
                    </p>

                    @break
                    @default
                @endswitch
                {{--<p class="secondary-color rate">
                    <i class="icon-star-full"></i>
                    <i class="icon-star-full"></i>
                    <i class="icon-star-full"></i>
                    <i class="icon-star-full"></i>
                    <i class="icon-star-half"></i>
                </p>
                --}}
                <hr>
                <p class="clearfix like">
                    <a class="pull-left" href="#">
                        @if($listing->watch_count > 0)
                        <i class="icon-eye"></i>
                       {{ $listing->watch_count }} defa incelendi .
                            @else
                            <i class="icon-new"></i>

                            Yeni ilan
                        @endif
                    </a>
                    <a href="details/{{$listing->unique_id}}" class="btn btn-primary btn-sm link-with-icon pull-right">İncele<i class=" icon-chevron-right"></i></a>
                </p>
            </div>
        </div>

    </div>

@endforeach