<tr>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Brüt Alan :</td>
    <td>{{ $listing->villa->gross_area }} m<sup>2</sup></td>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Net Alan :</td>
    <td>{{ $listing->villa->net_area }} m<sup>2</sup></td>
</tr>

<tr>
    <td><img class="micon" src="/svg/032-plan.svg" title=""/> Odalar :</td>
    <td>{{ $listing->villa->room }} Oda - {{ $listing->villa->saloon }} Salon</td>
    <td><img class="micon" src="/svg/049-bathroom.svg" title=""/> Banyo Sayısı :</td>
    <td>{{ $listing->villa->bath }} adet</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/023-balcony.svg" title=""/> Balkon :</td>
    <td>@if($listing->villa->balcony) Var @else Yok @endif</td>
    <td><img class="micon" src="/svg/003-closet.svg" title=""/> Eşya :</td>
    <td>@if($listing->villa->furnished) Var @else Yok @endif</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/035-heating-1.svg" title=""/> Isınma Tipi :</td>
    <td>{{ $listing->villa->heating_type->name }}</td>
    <td><img class="micon" src="/svg/038-building.svg" title=""/> Bina Yaşı :</td>
    <td>{{ $listing->villa->building_age }}</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/skyscraper.svg" title=""/> Kat Sayısı :</td>
    <td>{{ $listing->villa->floor_count }}</td>

    <td><img class="micon" src="/svg/deed.svg" title=""/> Tapu Tipi :</td>
    <td>{{ $listing->villa->deed_type->name }}</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/skyscraper.svg" title=""/> Yüzme Havuzu :</td>
    <td>{{ $listing->villa->villa_floor_count }}</td>

    <td><img class="micon" src="/svg/skyscraper.svg" title=""/> Güvenlik :</td>
    <td>{{ $listing->villa->security }}</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/deed.svg" title=""/>Yürüyüş Parkuru :</td>
    <td>{{ $listing->villa->walkingpath }}</td>
    <td><img class="micon" src="/svg/deed.svg" title=""/>Otopark :</td>
    <td>{{ $listing->villa->parking }}</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/deed.svg" title=""/>Oyun Alanı :</td>
    <td>{{ $listing->villa->playground }}</td>
    <td><img class="micon" src="/svg/deed.svg" title=""/>Spor Alanı :</td>
    <td>{{ $listing->villa->sportsarea }}</td>
</tr>


