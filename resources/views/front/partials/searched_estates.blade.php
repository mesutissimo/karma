<section class="probootstrap-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 section-heading probootstrap-animate">
            </div>
        </div>
        <div class="row mb70">
            @include('front.partials.listings_as_columns')


        </div>
    </div>
</section>