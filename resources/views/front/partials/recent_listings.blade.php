<section id="next-section">
    <div class="container-fluid">
        <div class="row">
            <div class="container">

                <div class="col-lg-12 col-md-12 mb70 section-heading probootstrap-animate">
                    <h2>En Son Eklenen Gayrimenkuller</h2>
                    <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and
                        Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the
                        coast of the Semantics, a large language ocean.</p>
                </div>

                <div class="row probootstrap-gutter0">

                    @foreach($listings as $listing)

                        <div class="col-md-4 col-sm-6">
                            <a href="portfolio-single.html" class="probootstrap-hover-overlay">
                                <div class="im">
                                    @foreach($listing->images as $image)
                                        <div><img data-u="image" class="img-responsive"
                                                  src="images/{{$image->resized_name}}" alt=""></div>
                                    @endforeach
                                </div>
                                <div class="probootstrap-text-overlay">
                                    <h3>{{ $listing->title }}</h3>
                                    <h4>{{ $listing->price }} {{ $listing->currency->symbol }}</h4>
                                    <p>Website, Photography</p>
                                </div>
                            </a>
                        </div>
                    @endforeach


                </div>

            </div>
        </div>

    </div>
</section>