<tr>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Brüt Alan :</td>
    <td>{{ $listing->flat->gross_area }} m<sup>2</sup></td>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Net Alan :</td>
    <td>{{ $listing->flat->net_area }} m<sup>2</sup></td>
</tr>
<tr>
    <td><img class="micon" src="/svg/032-plan.svg" title=""/> Odalar :</td>
    <td>{{ $listing->flat->room }} Oda - {{ $listing->flat->saloon }} Salon</td>
    <td><img class="micon" src="/svg/049-bathroom.svg" title=""/> Banyo Sayısı :</td>
    <td>{{ $listing->flat->bath }} adet</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/023-balcony.svg" title=""/> Balkon :</td>
    <td>@if($listing->flat->balcony) Var @else Yok @endif</td>
    <td><img class="micon" src="/svg/003-closet.svg" title=""/> Eşya :</td>
    <td>@if($listing->flat->furnished) Var @else Yok @endif</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/035-heating-1.svg" title=""/> Isınma Tipi :</td>
    <td>{{ $listing->flat->heating_type->name }}</td>
    <td><img class="micon" src="/svg/038-building.svg" title=""/> Bina Yaşı :</td>
    <td>{{ $listing->flat->building_age }}</td>
</tr>
<tr>
    <td><img class="micon" src="/svg/025-skyscraper.svg" title=""/> Kat Sayısı :</td>
    <td>{{ $listing->flat->building_floor_count }}</td>
    <td><img class="micon" src="/svg/025-skyscraper.svg" title=""/> Konut Katı :</td>
    <td>{{ $listing->flat->flats_floor }}</td>
</tr>
<tr>
    <td><img class="micon" src="/svg/deed.svg" title=""/> Tapu Tipi :</td>
    <td>{{ $listing->flat->deed_type->name }}</td>
    <td></td>
    <td></td>

</tr>