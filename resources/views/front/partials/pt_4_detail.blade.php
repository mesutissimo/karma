<tr>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Net Alan :</td>
    <td>{{ $listing->farm->net_area }} m<sup>2</sup></td>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Kapalı Alan :</td>
    <td>{{ $listing->farm->closed_area }} m<sup>2</sup></td>
</tr>
<tr>
    <td><img class="micon" src="/svg/045-location.svg" title=""/> Ada / Parsel:</td>
    <td>{{ $listing->farm->block }} / {{ $listing->farm->plot }}</td>
    <td></td>
    <td></td>
</tr>

<tr>
    <td><img class="micon" src="/svg/{{ $listing->farm->farm_type->symbol }}.svg" title=""/> Çiftlik Tipi:</td>
    <td>
        {{ $listing->farm->farm_type->name }}
    </td>
    <td><img class="micon" src="/svg/{{ $listing->farm->farm_subject->symbol }}.svg" title=""/> Besi / Süt:</td>
    <td>
        {{ $listing->farm->farm_subject->name }}
    </td>
</tr>
<tr>
    <td><i class="fas fa-sort-numeric-down"></i> Hayvan Sayısı :</td>
    <td><img class="micon" src="/svg/{{ $listing->farm->farm_type->symbol }}.svg" title=""/> x {{ $listing->farm->animal_count }}
    </td>
    <td><i class="fas fa-power-off"></i> Aktif Mi ? :</td>
    <td>@if($listing->farm->is_active) Evet @else Hayır @endif</td>
</tr>

