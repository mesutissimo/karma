<tr>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Brüt Alan :</td>
    <td>{{ $listing->land->gross_area }} m<sup>2</sup></td>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Net Alan :</td>
    <td>{{ $listing->land->net_area }} m<sup>2</sup></td>
</tr>
<tr>
    <td><img class="micon" src="/svg/032-plan.svg" title=""/> Ada / Parsel:</td>
    <td>{{ $listing->land->block }} / {{ $listing->land->plot }}</td>
    <td></td>
    <td></td>

</tr>
<tr>
    <td><img class="micon" src="/svg/014-crane.svg" title=""/> İmar Durumu :</td>
    <td>{{ $listing->land->zoning->name }}</td>
    <td></td>
    <td></td>
</tr>
