<section class="probootstrap-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 mb70 section-heading probootstrap-animate">
                <h2>Hizmetlerimiz</h2>
                <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
            </div>
        </div>
        <div class="row mb70">
            <div class="col-md-4 probootstrap-animate">
                <div class="probootstrap-box">
                    <div class="icon text-center"><i class="icon-tools2"></i></div>
                    <h3>Interface Design</h3>
                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                    <ul class="text-left with-icon colored">
                        <li><i class="icon-radio-checked"></i> <span>A small river named Duden</span></li>
                        <li><i class="icon-radio-checked"></i> <span>Place and supplie</span></li>
                        <li><i class="icon-radio-checked"></i> <span>Roasted parts of sentences</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 probootstrap-animate">
                <div class="probootstrap-box">
                    <div class="icon text-center"><i class="icon-desktop"></i></div>
                    <h3>User Experience</h3>
                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                    <ul class="text-left with-icon colored">
                        <li><i class="icon-radio-checked"></i> <span>A small river named Duden</span></li>
                        <li><i class="icon-radio-checked"></i> <span>Place and supplie</span></li>
                        <li><i class="icon-radio-checked"></i> <span>Roasted parts of sentences</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 probootstrap-animate">
                <div class="probootstrap-box">
                    <div class="icon text-center"><i class="icon-lightbulb"></i></div>
                    <h3>Product Strategy</h3>
                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                    <ul class="text-left with-icon colored">
                        <li><i class="icon-radio-checked"></i> <span>A small river named Duden</span></li>
                        <li><i class="icon-radio-checked"></i> <span>Place and supplie</span></li>
                        <li><i class="icon-radio-checked"></i> <span>Roasted parts of sentences</span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 probootstrap-animate">
                <p class="text-center">
                    <a href="#" class="btn btn-primary btn-lg btn-block" role="button">View All Services</a>
                </p>
            </div>
        </div>
    </div>
</section>
