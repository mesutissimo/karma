<tr>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Brüt Alan :</td>
    <td>{{ $listing->project->gross_area }} m<sup>2</sup></td>
    <td><img class="micon" src="/svg/034-area.svg" title=""/> Net Alan :</td>
    <td>{{ $listing->project->net_area }} m<sup>2</sup></td>
</tr>

<tr>
    <td><img class="micon" src="/svg/032-plan.svg" title=""/> Odalar :</td>
    <td>{{ $listing->project->room }} Oda - {{ $listing->project->saloon }} Salon</td>
    <td><img class="micon" src="/svg/049-bathroom.svg" title=""/> Banyo Sayısı :</td>
    <td>{{ $listing->project->bath }} adet</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/023-balcony.svg" title=""/> Balkon :</td>
    <td>@if($listing->project->balcony) Var @else Yok @endif</td>
    <td><img class="micon" src="/svg/003-closet.svg" title=""/> Eşya :</td>
    <td>@if($listing->project->furnished) Var @else Yok @endif</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/035-heating-1.svg" title=""/> Isınma Tipi :</td>
    <td>{{ $listing->project->heating_type->name }}</td>
    <td><img class="micon" src="/svg/038-building.svg" title=""/> Bina Yaşı :</td>
    <td>{{ $listing->project->building_age }}</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/skyscraper.svg" title=""/> Kat Sayısı :</td>
    <td>{{ $listing->project->floor_count }}</td>

    <td><img class="micon" src="/svg/deed.svg" title=""/> Tapu Tipi :</td>
    <td>{{ $listing->project->deed_type->name }}</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/pool.svg" title=""/> Yüzme Havuzu :</td>
    <td>{{ $listing->project->swimming_pool->name }}</td>

    <td><img class="micon" src="/svg/security.svg" title=""/> Güvenlik :</td>
    <td>{{ ($listing->project->security == 1) ? __('Var') : __('Yok') }}</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/deed.svg" title=""/> Yürüyüş Parkuru :</td>
    <td>{{ ($listing->project->walkingpath == 1) ? __('Var') : __('Yok') }}</td>
    <td><img class="micon" src="/svg/deed.svg" title=""/> Otopark :</td>
    <td>{{ ($listing->project->parking  == 1) ? __('Var') : __('Yok') }}</td>
</tr>

<tr>
    <td><img class="micon" src="/svg/deed.svg" title=""/> Oyun Alanı :</td>
    <td>{{ ($listing->project->playground  == 1) ? __('Var') : __('Yok') }}</td>
    <td><img class="micon" src="/svg/deed.svg" title=""/> Spor Alanı :</td>
    <td>{{ ($listing->project->sportsarea  == 1) ? __('Var') : __('Yok') }}</td>
</tr>


