<!-- START: footer -->
<footer role="contentinfo" class="probootstrap-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="probootstrap-footer-widget">
                    <h3>Karma GMG Hakkında</h3>
                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.
                        It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                    <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
                </div>
                <div class="probootstrap-footer-widget">
                    <h3>Connect With Us</h3>
                    <ul class="probootstrap-footer-social">
                        <li><a href=""><i class="icon-twitter"></i></a></li>
                        <li><a href=""><i class="icon-facebook"></i></a></li>
                        <li><a href=""><i class="icon-instagram2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="probootstrap-footer-widget">
                    <h3>{{ __('Popular Estates') }}</h3>
                    <ul class="probootstrap-product-list">
                        <li class="mb20">
                            <a href="#">
                                <figure><img src="img/img_3.jpg" alt="Free Bootstrap Template by uicookies.com"
                                             class="img-responsive"></figure>
                                <div class="text">
                                    <h4>River named Duden flows</h4>
                                    <p>A small river named Duden flows by their place</p>
                                    <p class="secondary-color rate"><i class="icon-star-full"></i><i
                                                class="icon-star-full"></i><i class="icon-star-full"></i><i
                                                class="icon-star-full"></i><i class="icon-star-half"></i></p>
                                </div>
                            </a>
                        </li>
                        <li class="mb20">
                            <a href="#">
                                <figure><img src="img/img_4.jpg" alt="Free Bootstrap Template by uicookies.com"
                                             class="img-responsive"></figure>
                                <div class="text">
                                    <h4>River named Duden flows</h4>
                                    <p>A small river named Duden flows by their place</p>
                                    <p class="secondary-color rate"><i class="icon-star-full"></i><i
                                                class="icon-star-full"></i><i class="icon-star-full"></i><i
                                                class="icon-star-full"></i><i class="icon-star-half"></i></p>
                                </div>
                            </a>
                        </li>
                        <li class="mb20">
                            <a href="#">
                                <figure><img src="img/img_5.jpg" alt="Free Bootstrap Template by uicookies.com"
                                             class="img-responsive"></figure>
                                <div class="text">
                                    <h4>River named Duden flows</h4>
                                    <p>A small river named Duden flows by their place</p>
                                    <p class="secondary-color rate"><i class="icon-star-full"></i><i
                                                class="icon-star-full"></i><i class="icon-star-full"></i><i
                                                class="icon-star-full"></i><i class="icon-star-half"></i></p>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="probootstrap-footer-widget">
                    <h3>{{ __('Recent Estates') }}</h3>
                    <ul class="probootstrap-blog-list">
                        @foreach($recent_listings as $recent_listing)

                            <li>
                                <a href="details/{{$recent_listing->unique_id}}">
                                    <figure><img src="images/{{$recent_listing->images[0]->resized_name}}"
                                                 class="img-responsive"></figure>
                                    <div class="text">
                                        <h4>{{ $recent_listing->title }}</h4>
                                        <p>{{ $recent_listing->price }} {{ $recent_listing->currency->symbol }}</p>
                                    </div>
                                </a>
                            </li>

                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="row mt40">
            <div class="col-md-12 text-center">
                <p>
                    <small>&copy; {{ date("Y") }}<br> <a href="https://www.karmagmg.com/" target="_blank">{{ __('All Rights Reserved') }}</a>. <br>
                        <a href="#" class="js-backtotop">{{ __('Return to Top') }}</a>
                </p>
            </div>
        </div>
    </div>
</footer>
