<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Karma GMG') }}</title>

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="{{ asset('css/styles-merged.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.min.js"></script>
    <script src="js/vendor/respond.min.js"></script>
    <![endif]-->

    <!-- JS -->
    <script src="{{ url('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>

    <script defer src="{{ url('https://use.fontawesome.com/releases/v5.5.0/js/all.js') }}"
            integrity="sha384-GqVMZRt5Gn7tB9D9q7ONtcp4gtHIUEW/yG7h98J7IpE3kpi+srfFyyB/04OV6pG0"
            crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>

</head>
<body>

<div id="">
    <header role="banner" class="probootstrap-header">

        <div class="container-fluid">
            <a href="{{ url('/') }}" class="probootstrap-logo">
                <img src="{{ asset('/img/karma_logo.png') }}" class="logo-image" alt="Karma Gayrimenkul Geliştirme">
            </a>

            <a href="#" class="probootstrap-burger-menu visible-xs"><i>Menu</i></a>
            <div class="mobile-menu-overlay"></div>


            <nav role="navigation" class="probootstrap-nav hidden-xs">

                <ul class="probootstrap-main-nav">

                    {{-- Language Selector Menu --}}

                    <li><a href="{{ url('flats') }}">{{ __('Flat') }}</a></li>
                    <li><a href="{{ url('villas') }}">{{ __('Villa') }}</a></li>
                    <li><a href="{{ url('lands') }}">{{ __('Land') }}</a></li>
                    <li><a href="{{ url('landsrff') }}">{{ __('Land In Return For Flat') }}</a></li>
                    <li><a href="{{ url('farms') }}">{{ __('Farm') }}</a></li>
                    <li><a href="{{ url('projects') }}">{{ __('Project') }}</a></li>
                    <li class="dropdown">
                        <a href="#" role="button" data-toggle="modal" data-target="#language-selector"
                           aria-expanded="false">
                            <img class="mflag" src="./flags/{{ Config::get('app.locale') }}.svg" title=""/>
                        </a>
                        <ul class="dropdown-menu">
                            <li><img class="mflag" src="./flags/en.svg" title=""/></li>
                            <li><img class="mflag" src="./flags/sa.svg" title=""/></li>
                            <li><img class="mflag" src="./flags/tr.svg" title=""/></li>
                        </ul>
                    </li>


                </ul>


                <div class="extra-text visible-xs">
                    <a href="#" class="probootstrap-burger-menu"><i>Menu</i></a>
                    <h5>Sosyal Ağlar</h5>
                    <ul class="social-buttons">
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-instagram2"></i></a></li>
                    </ul>
                    <p>
                        <small>&copy; {{ date("Y") }}<br> {{ __('All Rights Reserved') }}.</small>
                    </p>
                </div>
            </nav>

        </div>


    </header>

    @yield('content')

    <div id="language-selector" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('Choose your language') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <a href="{{ route('set.language', 'en') }}"><img class="micon" src="./flags/en.svg" title=""/></a>
                    <a href="{{ route('set.language', 'sa') }}"><img class="micon" src="./flags/sa.svg" title=""/></a>
                    <a href="{{ route('set.language', 'tr') }}"><img class="micon" src="./flags/tr.svg" title=""/></a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Scripts -->

<script src="{{ asset('js/scripts.min.js') }}"></script>
<script src="{{ asset('js/main.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>

<script type="text/javascript"
        src="{{url('//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js')}}"></script> {{-- Slick image slider --}}
<script type="text/javascript"
        src="{{url('https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.1.1/pdfobject.min.js')}}"></script> {{-- Slick image slider --}}

<script>
    $(document).ready(function () {
        $('.im').slick({
            autoplay: true,
            arrows: false,
            centerMode: true,
            variableWidth: true
        });

        $('#slider').slick({
            autoplay: true,
            arrows: false,
        })
    });
</script>

</body>
</html>


