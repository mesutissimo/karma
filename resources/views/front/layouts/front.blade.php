<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Karma GMG') }}</title>


    <!-- Styles -->
    {{--
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/gmg.css') }}" rel="stylesheet">
    --}}
    <link href="{{ asset('css/styles-merged.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>


    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.min.js"></script>
    <script src="js/vendor/respond.min.js"></script>
    <![endif]-->

    <!-- JS -->
    <script src="{{ url('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>

    <script defer src="{{ url('https://use.fontawesome.com/releases/v5.5.0/js/all.js') }}" integrity="sha384-GqVMZRt5Gn7tB9D9q7ONtcp4gtHIUEW/yG7h98J7IpE3kpi+srfFyyB/04OV6pG0" crossorigin="anonymous"></script>
</head>
<body>
    <div id="">
        <header role="banner" class="probootstrap-header ">
            <div class="container-fluid">
                <a href="{{ url('/') }}" class="probootstrap-logo"><span> {{ config('app.name', 'Karma GMG') }}</span></a>

                <a href="#" class="probootstrap-burger-menu visible-xs" ><i>Menu</i></a>
                <div class="mobile-menu-overlay"></div>

                <nav role="navigation" class="probootstrap-nav hidden-xs">
                    <ul class="probootstrap-main-nav">
                        <li><a href="{{ url('/flats') }}">Daire</a></li>
                        <li><a href="{{ url('/lands') }}">Arsa</a></li>
                        <li><a href="{{ url('/landsrff') }}">Kat Karşılığı Arsa</a></li>
                        <li><a href="{{ url('/farms') }}">Çiftlik</a></li>
                    </ul>
                    <div class="extra-text visible-xs">
                        <a href="#" class="probootstrap-burger-menu"><i>Menu</i></a>
                        <h5>Sosyal Ağlar</h5>
                        <ul class="social-buttons">
                            <li><a href="#"><i class="icon-twitter"></i></a></li>
                            <li><a href="#"><i class="icon-facebook"></i></a></li>
                            <li><a href="#"><i class="icon-instagram2"></i></a></li>
                        </ul>
                        <p><small>&copy; 2018<br> Tüm Hakları Saklıdır.</small></p>
                    </div>
                </nav>
            </div>
        </header>

        @yield('content')
    </div>

    <!-- Scripts -->

    <script src="{{ asset('js/scripts.min.js') }}"></script>
    <script src="{{ asset('js/main.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
{{--
    <script src="{{ asset('js/app.js') }}"></script>

    --}}
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<!--
    <script src="{{ asset('js/jssor.slider.min.js') }}"></script>
    <script>

        jQuery(document).ready(function ($) {

            let options = {
                $FillMode: 2,                                       //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actual size, 5 contain for large image, actual size for small image, default value is 0
                $AutoPlay: 1,                                       //[Optional] Auto play or not, to enable slideshow, this option must be set to greater than 0. Default value is 0. 0: no auto play, 1: continuously, 2: stop at last slide, 4: stop on click, 8: stop on user navigation (by arrow/bullet/thumbnail/drag/arrow key navigation)
                $Idle: 4000,                                        //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $ArrowKeyNavigation: 1,   			                //[Optional] Steps to go for each navigation request by pressing arrow key, default value is 1.
                $SlideEasing: $Jease$.$OutQuint,                    //[Optional] Specifies easing for right to left animation, default value is $Jease$.$OutQuad
                $SlideDuration: 800,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide, default value is 20
                //$SlideWidth: 60,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 30,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $Cols is greater than 1, or parking position is not 0)

                $BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $SpacingX: 8,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $Orientation: 1                                //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                },

                $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2                                 //[Optional] Steps to go for each navigation request, default value is 1
                }
            };
/*
            let sliders = $('.sl');

            for(i=1;i< sliders.length; i++){
                let slider = new $JssorSlider$("jssor_"+ i , options);
                ScaleSlider(i,slider);

            }

            function ScaleSlider(sliderNo,slider) {
                let sl = $('#jssor_'+sliderNo );
                let bodyWidth = sl.parent().width();
                if (bodyWidth)
                   slider.$ScaleWidth(bodyWidth);
                else
                    window.setTimeout(30);
            }
*/



        });

    </script>
    -->
    <script>
        $(document).ready(function() {
                $('.im').slick({
                    autoplay :true,
                    arrows :false,
                    centerMode : true,
                    variableWidth: true

                })
            });
    </script>
</body>
</html>


