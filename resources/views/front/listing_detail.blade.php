@extends('front.layouts.front2')

@section('content')
    <section class="probootstrap-section">
        <div class="container">
            <div class="col-md-6 col-md-push-6">
                <h2>{{ $listing->title }}</h2>
                <h3>
                    {{ number_format($listing->price,null,null,'.') }} {{ $listing->currency->symbol }}
                    <p class="pull-right">
                        <a>
                            <small>{{$listing->district->name}} / <strong>{{$listing->province->name}}</strong>
                                <i class="fas fa-map-marker-alt"></i>
                            </small>
                        </a>
                    </p>
                </h3>


                <table class="table table-condensed table-striped probootstrap-black-color">
                    <thead>
                    <tr>
                        <td colspan="4"><h4 class="text-danger pull-right"><strong>Teknik Ayrıntılar</strong></h4></td>
                    </tr>
                    </thead>
                    <tbody>

                    @include('front.partials.pt_'. $listing->property_type->id . '_detail')

                    </tbody>
                    @if($listing->precedent_value > 0)
                        <tfoot>
                        <tr>
                            <td><img class="micon" src="{{ asset('svg/skyscraper.svg') }}"/> Emsal Değeri</td>
                            <td colspan="3">{{ number_format($listing->precedent_value,null,null,'.') }} {{$listing->currency->symbol}}</td>
                        </tr>
                        </tfoot>
                    @endif
                </table>
                @if($listing->details != '')
                    <table class="table table-condensed table-striped probootstrap-black-color">
                        <thead>
                        <tr>
                            <td><h4 class="text-danger pull-right"><strong>Detaylar</strong></h4>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                {{ $listing->details }}

                            </td>
                        </tr>
                        </tbody>
                    </table>

                @endif
                @if($listing->presentation != null)

                    <p>
                        <a href="{{ route('download_presentation', $listing->unique_id) }}" role="button"
                           class="btn btn-primary">Sunumu İndir</a>
                    </p>

                @endif
            </div>

            <div class="col-md-6 col-md-pull-6">
                <div class="im" style="margin-top: 4em">
                    @foreach($listing->images as $image)
                        <a href="/images/{{ $image->resized_name }}" class="image-popup">
                            <img src="/images/{{ $image->resized_name }}" alt="" class="img-responsive">
                        </a>
                    @endforeach
                </div>

            </div>

        </div>
    </section>


@endsection