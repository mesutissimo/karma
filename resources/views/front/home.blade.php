@extends('front.layouts.front2')

@section('content')
    <!-- START: section -->
    <section class="probootstrap-section">

        <div id="carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                @foreach($slider_images as $key=>$simage)
                    <div class="carousel-item @if($key == 0) active @endif">
                        <img class="d-block w-100 img-responsive" src="img/slider/{{ $simage->filename }}">
                    </div>
                @endforeach
            </div>
        </div>

        <div id="search" class="search">
            <form action="{{ route('searchwith') }}" method="post">

            <select name="property_type" id="property_type">
                @foreach($property_types as $pt)
                <option value="{{ $pt->id }}">{{ __($pt->name) }}</option>
                @endforeach
            </select>
                {{ csrf_field() }}
                <input class="search-box" name="search_query"/>
                <button class="search-button">
                    <i class="fas fa-search"></i> {{ __('Search') }}
                </button>
            </form>
        </div>

    </section>

    @include('front.partials.all_listings')

    <!-- START: footer -->
    @include('front.footer')

    <script>
        let word = 'naber';
        let inputs = [];
        document.onkeypress = function (e) {
            e = e || window.event;
            let mWord = '';
            inputs.push(String.fromCharCode(e.keyCode));
            if (inputs.length == word.length) {
                for (i = 0; i < inputs.length; i++) {
                    mWord = mWord + inputs[i];
                }
            }
            if (mWord == word) {
                window.location.href = '/adminhome';
            }
        };
    </script>

    <script>
        $('.carousel').carousel({
            interval: 5000,
            pause: false
        })
    </script>
@endsection
