@extends('front.layouts.front2')

@section('content')

    @include('front.partials.farm_listings')

    @include('front.footer')
@endsection
