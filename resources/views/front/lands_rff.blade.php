@extends('front.layouts.front2')

@section('content')

    @include('front.partials.land_listings')

    @include('front.footer')
@endsection
