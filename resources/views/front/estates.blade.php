@extends('front.layouts.front2')

@section('content')

    @include('front.partials.searched_estates')
    @include('front.footer')

@endsection
