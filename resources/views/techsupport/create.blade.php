@extends('layouts.app')

@section('content')

    <div class="col-sm-6 col-sm-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Destek İste ya da Görüş Bildir</h3>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>

            <div class="panel-body">
                <form id="issue_form" method="POST" action="{{ route('techsupport.store') }}"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="subject">Yardım Konusu :</label>
                        <input class="form-control" name="subject">
                    </div>
                    <div class="form-group">
                        <label for="subject">Detaylar :</label>
                        <textarea class="form-control" name="detail"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Gönder</button>
                    </div>

                </form>
            </div>
        </div>

    </div>

@endsection