@extends('layouts.app')

@section('content')

    <div class="col-sm-6 col-sm-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                    <h3>Destek ve Görüş İstekleriniz</h3>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <h3 class="pull-right">
                    <a href="{{route('techsupport.create')}}"
                       class="btn btn-xs btn-primary pull-right">
                        Yeni
                    </a>
                </h3>
            </div>
            <div class="panel-body">
                @if(count($issues) != 0)
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Konu</th>
                        <th class="text-right" style="width: 10%">Durum</th>
                        <th class="text-right" style="width: 20%">Bildirim Zamanı</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($issues as $issue)
                    <tr>
                        <td>{{ $issue->subject }}</td>
                        <td class="text-right"> {{ $issue->status->name }}</td>
                        <td class="text-right"> {{ $issue->created_at }}</td>

                    </tr>
                    </tbody>
                    @endforeach
                </table>
                    @else
                    <p>Kayıt bulunamadı</p>
                    @endif
            </div>
        </div>

    </div>

@endsection