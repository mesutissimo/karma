@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"></div>

                    <div class="panel-body">
                        <h4>{{ __ ('En çok görüntülenen ilanlar') }}</h4>

                        <div class="col-sm-12">
                            <table class="table table-striped table-bordered table-responsive table-condensed">

                                <thead>
                                <tr>
                                    <th>{{ __('İlan Başlığı') }}</th>
                                    <th>{{ __('İlan Numarası') }}</th>
                                    <th>{{ __('Emlak Tipi') }}</th>
                                    <th>{{ __('Konum') }}</th>
                                    <th>{{ __('Fiyat') }}</th>
                                    <th>{{ __('İncelenme Sayısı') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($listings as $listing)
                                    <tr>
                                        <td> {{ $listing->title }} </td>
                                        <td> {{ $listing->unique_id  }} </td>
                                        <td> {{ $listing->property_type->name }} </td>
                                        <td> {{ $listing->province->name . ' - ' . $listing->district->name }} </td>
                                        <td> {{ $listing->price . ' ' . $listing->currency->symbol }} </td>
                                        <td> {{ $listing->watch_count }} </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>


                    </div>
                    <div class="panel-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
