@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>{{ __($listing_type->name) }} {{ __($property_type->name) }} Ekle # {{ $unique_id }}</h4>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="panel-body">
                        <div class="col-sm-4">
                            {{-- IMAGE SECTION --}}
                            @include('listings.images')

                        </div>

                        <form id="listing_form" method="POST" action="{{ route('listings.store') }}"
                              enctype="multipart/form-data">

                            {{ csrf_field() }}
                            {{-- PREDEFINED OR HIDDEN INPUTS --}}

                            <input type="hidden" name="unique_id" value="{{ $unique_id }}">
                            <input type="hidden" name="listing_type" value="{{ $listing_type->id }}">
                            <input type="hidden" name="property_type" value="{{ $property_type->id }}">
                            <div class="col-sm-8">
                                <div class="col-sm-12">
                                    <div class="panel panel-info">
                                        <div class="panel-heading ">
                                            <label>Genel Bilgiler</label>
                                        </div>

                                        <div class="panel-body">

                                            {{-- Listing Title --}}

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"> İlan Başlığı</span>
                                                        <input id="title" class="form-control"
                                                               placeholder="İlan Başlığı Giriniz"
                                                               name="title"/>

                                                    </div>
                                                </div>
                                            </div>

                                            {{-- Listing Address --}}

                                            <div class="col-sm-5">
                                                <label>Adres</label>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="">İl</span>
                                                        <select name="province" class="form-control">
                                                            @foreach($provinces as $province)
                                                                <option value="{{ $province->id }}"
                                                                        @if($province->id == 40) selected @endif>
                                                                    {{ $province->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">

                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="">İlçe</span>
                                                        <select name="district" class="form-control">
                                                            @foreach($districts as $district)
                                                                <option value="{{ $district->id }}"
                                                                        @if($district->id == 456) selected @endif>
                                                                    {{ $district->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- Listing Location --}}

                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <label for="">Konum</label>
                                                    <div class="input-group">
                                                        <div class="btn btn-light btn-info" data-toggle="modal"
                                                             data-target="#mapModal">
                                                            <i class="fas fa-map-marker-alt"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- Listing Price --}}

                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label for="">Gayrimenkul Fiyatı</label>
                                                    <div class="input-group">
                                                        <input type="number" min="1" id="title" class="form-control"
                                                               placeholder="Fiyat Girin" name="price"
                                                               style=" height:36px; width:65%">
                                                        <select name="currency" class="input-group-addon"
                                                                style=" height:36px; width:35%">
                                                            @foreach($currencies as $currency)
                                                                <option value="{{ $currency->id }}">{{ $currency->symbol }} {{ $currency->code }}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="equal">Emsal Değeri</label>
                                                    <input type="number" id="title" class="form-control"
                                                           name="precedent_value">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <p id="filename">İlanın ayrıntılı bilgilerini içeren sunumu
                                                        ekleyin</p>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="file" name="presentation" id="file"/>
                                                    <label class="btn btn-xs btn-success pull-right" for="file">
                                                        Sunum Dosyası Ekle
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                {{-- Listing Type Dedicated Details --}}

                                @include('listings.listingspecial.pt'.$listing_type->id.'_'.$property_type->id)

                            </div>


                            <div class="col-sm-12 ">

                                <button type="reset" class="btn btn-danger">
                                    <i class="fas fa-times"></i> İptal
                                </button>

                                <button type="submit" id="save_listing" class="btn btn-primary pull-right">
                                    <i class="fas fa-check"></i> Ekle
                                </button>

                            </div>


                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
    @include('listings.location')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function () {
            $('select[name="province"]').on('change', function () {
                var provinceId = $(this).val();
                if (provinceId) {
                    $.ajax({
                        url: '/districts/get/' + provinceId,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {

                            $('select[name="district"]').empty();

                            $.each(data, function (key, value) {

                                $('select[name="district"]').append('<option value="' + key + '">' + value + '</option>');

                            });
                        },

                    });
                } else {
                    $('select[name="district"]').empty();
                }

            });

        });

    </script>
    <script>


        Dropzone.autoDiscover = false;

        let idrop = new Dropzone("#imgDropzone", {
            url: "{{ route('image_upload') }}",
            maxFilesize: "5",
            //addRemoveLinks: true,
            previewsContainer: '#preview',
            previewTemplate: document
                .querySelector('#tpl')
                .innerHTML,
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 15,

            success: function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('get_images') }}",
                    data: {
                        uid: " {{ $unique_id }} "
                    },

                    contentType: "text/json; charset=utf-8",
                    dataType: "text",
                    success: function (msg) {

                    }
                });
            },


            sending: function (file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
                formData.append("listing_uid", "{{ $unique_id }}");
                formData.append("target", "listing");
            },

            completemultiple: function () {
                $('#listing_form').submit();
                console.log("Images Uploaded")
            }
        });


        idrop.on("addedfile", function (file) {

            file.previewElement.addEventListener("click", (e) => {
                let target = $(e.target);
                console.log(target);
                if (target.is('#remove')) {
                    idrop.removeFile(file);
                }


            })
        });

        $('#save_listing').on('click', function () {
            event.preventDefault();

            if(idrop.getAcceptedFiles().length > 0) {
                showLoading();
                idrop.processQueue();
                console.log("Images Uploading")
            } else {
                alert("En az bir resim eklemeniz gerekiyor");
            }

        })

    </script>

    {{-- Presentation File Form Element --}}
    <script>
        const file_element = $('#file');
        $('#file').on('change', () => {

            if(file_element[0].files.length > 0) {

                $('#filename').html(' <i class="fas fa-file-pdf"></i> ' + file_element[0].files[0].name)
            } else {

                $('#filename').html('İlanın ayrıntılı bilgilerini içeren sunumu ekleyin')

            }
        })
    </script>

@endsection
