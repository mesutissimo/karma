@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    {{--    <h4>{{ $listing->listing_type->name }} {{ $property_type->name }} Ekle # {{ $$listing->unique_id }}</h4> --}}
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>



                    <div class="panel-body">
                        <div class="col-sm-4">
                            {{-- IMAGE SECTION --}}
                            @include('listings.images')

                        </div>

                        <form id="listing_form" method="POST" action="{{ route('listings.update',$listing->id)  }}" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            {{-- PREDEFINED OR HIDDEN INPUTS --}}

                            <input type="hidden" name="unique_id" value="{{ $listing->unique_id }}">
                            <input type="hidden" name="listing_type" value="{{ $listing->listing_type_id }}">
                            <input type="hidden" name="property_type" value="{{ $listing->property_type_id }}">

                            <div class="col-sm-8">

                                <div class="col-sm-12">
                                    <div class="panel panel-info">
                                        <div class="panel-heading ">
                                            <label>Genel Bilgiler</label>
                                        </div>

                                        <div class="panel-body">

                                            {{-- Listing Title --}}

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"> İlan Başlığı</span>
                                                        <input id="title" class="form-control"
                                                               placeholder="İlan Başlığı Giriniz"
                                                               value="{{$listing->title}}"
                                                               name="title"/>

                                                    </div>
                                                </div>
                                            </div>

                                            {{-- Listing Address --}}

                                            <div class="col-sm-5">
                                                <label>Adres</label>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="">İl</span>
                                                        <select name="province" class="form-control">
                                                            @foreach($provinces as $province)
                                                                <option value="{{ $province->id }}"
                                                                        @if($province->id == $listing->province->id) selected @endif>
                                                                    {{ $province->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">

                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="">İlçe</span>
                                                        <select name="district" class="form-control">
                                                            @foreach($districts as $district)
                                                                <option value="{{ $district->id }}"
                                                                        @if($district->id == $listing->district->id) selected @endif>
                                                                    {{ $district->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- Listing Location --}}

                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <label for="">Konum</label>
                                                    <div class="input-group">
                                                        <div class="btn btn-light btn-info" data-toggle="modal"
                                                             data-target="#mapModal">
                                                            <i class="fas fa-map-marker-alt"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- Listing Price --}}

                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label for="">Gayrimenkul Fiyatı</label>
                                                    <div class="input-group">
                                                        <input type="number" min="1" id="title" class="form-control"
                                                               placeholder="Fiyat Girin" name="price"
                                                               value="{{$listing->price}}"
                                                               style=" height:36px; width:65%">
                                                        <select name="currency" class="input-group-addon"
                                                                style=" height:36px; width:35%">
                                                            @foreach($currencies as $currency)
                                                                <option value="{{ $currency->id }}"
                                                                @if($currency->id == $listing->currency->id) selected @endif>
                                                                    {{ $currency->symbol }} {{ $currency->code }}
                                                                </option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="equal">Emsal Değeri</label>
                                                    <input type="number" id="title" class="form-control"
                                                           name="precedent_value" value="{{ $listing->precedent_value }}">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <p id="filename">
                                                        @if(isset($listing->presentation))
                                                           <a href="{{ route('download_presentation',$listing->unique_id) }}" class="btn btn-xs btn-primary"><i class="fas fa-download"></i> Sunumu indir</a>
                                                            <button type="button" class="btn btn-xs btn-danger"   data-toggle="modal" data-target="#modal_delete_presentation"><i class="fas fa-trash"></i> Sunumu sil</button>
                                                            @else
                                                            İlanın ayrıntılı bilgilerini içeren sunumu ekleyin
                                                        @endif
                                                    </p>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="file" name="presentation" id="file"/>
                                                    <label id="file_button" class="btn btn-xs btn-success pull-right" for="file">
                                                        @if(isset($listing->presentation))
                                                        Sunum Dosyası Değiştir
                                                            @else
                                                            Sunum Dosyası Ekle
                                                        @endif
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                {{-- Listing Type Dedicated Details --}}

                                @include('listings.listingspecial.pt'.$listing->listing_type_id .'_'.$listing->property_type_id)

                            </div>

                            <div class="col-sm-12 ">

                                <button type="reset" class="btn btn-danger">
                                    <i class="fas fa-times"></i> İptal
                                </button>

                                <button type="submit" id="edit_listing" class="btn btn-primary pull-right">
                                    <i class="fas fa-check"></i> Düzenle
                                </button>

                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('listings.location')
    @include('listings.presentation_delete_alert')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function () {
            $('select[name="province"]').on('change', function () {
                var provinceId = $(this).val();
                if (provinceId) {
                    $.ajax({
                        url: '/districts/get/' + provinceId,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {

                            $('select[name="district"]').empty();

                            $.each(data, function (key, value) {

                                $('select[name="district"]').append('<option value="' + key + '">' + value + '</option>');

                            });
                        },

                    });
                } else {
                    $('select[name="district"]').empty();
                }

            });

        });

    </script>
    <script>


        let removedFiles = [];
        Dropzone.autoDiscover = false;

        let idrop = new Dropzone("#imgDropzone", {
            url: "{{ route('image_upload') }}",
            maxFilesize: "2",
            previewsContainer: '#preview',
            previewTemplate: document
                .querySelector('#tpl')
                .innerHTML,
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 15,

            init: function() {
                myDropzone = this;
                $.ajax({
                    url: '{{ route('get_images') }}',
                    type: 'post',
                    data: {
                        uid: " {{ $listing->unique_id }} ",
                        target: "listing"
                    },
                    dataType: 'json',
                    success: function(response){

                        $.each(response, function(key,value) {
                            let mockFile = { name: value.resized_name};
                            console.log(myDropzone.file);

                            myDropzone.emit("addedfile", mockFile);
                            myDropzone.emit("thumbnail", mockFile, '/images/'+ value.resized_name);
                            myDropzone.emit("complete", mockFile);

                        });

                    }
                });
            },

            success: function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('get_images') }}",
                    data: {
                        uid: " {{ $listing->unique_id }} "
                    },

                    contentType: "text/json; charset=utf-8",
                    dataType: "text",
                    success: function (msg) {

                    }
                });
            },


            sending: function (file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
                formData.append("listing_uid", "{{ $listing->unique_id }}");
                formData.append("target", "listing");

            },

            completemultiple: function () {
                $('#listing_form').submit();
                console.log("Images Uploaded")
            }
        });


        idrop.on("addedfile", function (file) {

            file.previewElement.addEventListener("click", (e) => {
                let target = $(e.target);
                if (target.is('#remove')) {
                    idrop.removeFile(file);
                }

            })
        });

        idrop.on('removedfile', function (file) {
            removedFiles.push({ 'name' : file.name});
        });

        $('#edit_listing').on('click', function () {
            event.preventDefault();
            showLoading();
            deleteRemovedFiles();
            idrop.processQueue();
            if(idrop.getQueuedFiles().length > 0) {
                idrop.processQueue();
            } else {
                $('#listing_form').submit();
            }
        });


        function deleteRemovedFiles() {

            if(removedFiles.length > 0) {

                let rfJson = JSON.stringify(removedFiles);
                console.log(rfJson)
                $.ajax({
                    type: "POST",
                    url: " {{ route('delete_image') }}",
                    data: {
                        removed_image: rfJson,
                        uid: '{{ $listing->unique_id }}',
                        target: 'listing'
                    },
                    dataType: "json",
                    success: function (msg) {
                        console.log(msg);
                    }
                });
                removedFiles = [];


            }

        }

    </script>

    {{-- Presentation File Form Element --}}
    <script>
        const file_element = $('#file');
        let defaultHtml = `
        <a href="{{ route('download_presentation',$listing->unique_id) }}"><i class="fas fa-file-pdf"></i> Sunumu indir</a>
        `;
        $('#file').on('change', () => {

            if(file_element[0].files.length > 0) {

                $('#filename').html(' <i class="fas fa-file-pdf"></i> ' + file_element[0].files[0].name)
            } else {
                @if(isset($listing->presentation))
                $('#filename').html(defaultHtml)
                @else
                $('#filename').html('İlanın ayrıntılı bilgilerini içeren sunumu ekleyin')
                @endif


            }
        })

        {{-- Delete Presentation File --}}

        function delete_presentation() {
            event.preventDefault();
            console.log('DeleteOP')
            $.ajax({
                type: "GET",
                url: "{{ route('delete_presentation',$listing->unique_id) }}",
                contentType: "text/json; charset=utf-8",
                dataType: "text",
                success: function (msg) {

                    $('#modal_delete_presentation').modal('hide');
                    $('#filename').html('İlanın ayrıntılı bilgilerini içeren sunumu ekleyin');
                    $('#file_button').html('Sunum Ekle');

                }
            });
        }

    </script>


@endsection
