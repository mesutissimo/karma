<div class="col-sm-12">
    <div class="panel panel-success">
        <div class="panel-heading ">
            <label>Detaylar</label>

        </div>
        <div class="panel-body">
            <div class="col-sm-3">
                <label for="gross_area">Brüt Alan:</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="gross_area"
                               @if($edit) value="{{$listing->flat->gross_area }}" @endif />
                        <span class="input-group-addon">m<sup>2</sup></span>
                    </div>

                </div>
            </div>
            <div class="col-sm-3">
                <label for="net_area">Net Alan:</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="net_area"
                               @if($edit) value="{{$listing->flat->net_area }}" @endif />
                        <span class="input-group-addon">m<sup>2</sup></span>
                    </div>

                </div>
            </div>

            <div class="col-sm-6">
                <label>Oda Sayısı :</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="room"
                               @if($edit) value="{{$listing->flat->room }}" @endif />
                        <span class="input-group-addon">Oda</span>
                        <input type="text" class="form-control" name="saloon"
                               @if($edit) value="{{$listing->flat->saloon }}" @endif />
                        <span class="input-group-addon">Salon</span>
                        <input type="text" class="form-control" name="bath"
                               @if($edit) value="{{$listing->flat->bath }}" @endif />
                        <span class="input-group-addon">Banyo</span>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="building_age">Bina Yaşı :</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="building_age"
                               @if($edit) value="{{$listing->flat->building_age }}" @endif />
                        <span class="input-group-addon">Yıl</span>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="flats_floor">Bulunduğu Kat :</label>
                <div class="form-group">
                    <input type="text" class="form-control"
                           id="flats_floor" name="flats_floor"
                           @if($edit) value="{{$listing->flat->flats_floor }}" @endif />
                </div>
            </div>

            <div class="col-sm-3">
                <label for="floor_count">Bina Kat Sayısı :</label>
                <div class="form-group">
                    <input type="text" class="form-control"
                           id="floor_count" name="building_floor_count"
                           @if($edit) value="{{$listing->flat->building_floor_count }}" @endif />
                </div>
            </div>

            <div class="col-sm-3">
                <label for="heating_type">Isınma Tipi :</label>
                <div class="form-group">
                    <select class="form-control" name="heating_type" id="heating_type">
                        <option value="0">Seçiniz</option>
                        @foreach($heating_types as $heating_type)
                            <option value="{{$heating_type->id}}"
                                    @if($edit && $heating_type->id == $listing->flat->heating_type->id) selected @endif >
                                {{ $heating_type->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-check-inline">

                    <label for="balcony">Balkon :</label>
                    <input type="checkbox" class="form-check-input"
                           name="balcony" id="balcony"
                           value="1"
                           data-toggle="toggle"
                           data-on="Var" data-off="Yok"
                           data-onstyle="success" data-offstyle="danger" data-size="mini"
                           @if($edit && $listing->flat->balcony) checked @endif>

                    <label for="furnished">Eşyalı :</label>
                    <input type="checkbox" class="form-check-input" name="furnished" id="furnished"
                           data-toggle="toggle"
                           data-on="Evet" data-off="Hayır"
                           data-onstyle="success" data-offstyle="danger" data-size="mini"
                           @if($edit && $listing->flat->furnished) checked @endif>

                </div>
            </div>

            <div class="col-sm-6">
                {{--
                <div class="form-check-inline">

                   <label for="credit_eligibilty">Krediye Uygun :</label>
                   <input type="checkbox" class="form-check-input" name="credit_eligibilty" id="credit_eligibilty"
                          value="1"
                          data-toggle="toggle"
                          data-on="Evet" data-off="Hayır"
                          data-onstyle="success" data-offstyle="danger" data-size="mini" @if($edit && $heating_type->id == $listing->$heating_type_id) checked @endif>

                </div>
                --}}
            </div>


            <div class="col-sm-3">
                <label for="deed_type">Tapu Durumu :</label>
                <div class="form-group">
                    <select type="text" class="form-control" name="deed_type">
                        @foreach($deed_types as $deed_type)
                            <option value="{{ $deed_type->id }}"
                                    @if($edit && $deed_type->id == $listing->flat->deed_type->id) selected @endif >
                                {{ $deed_type->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="col-sm-12">
                <label for="details">Açıklama :</label>
                <div class="form-group">
      <textarea type="text" class="form-control"
                name="details"></textarea>
                </div>
            </div>

        </div>

    </div>
</div>