<div class="col-sm-12">
    <div class="panel panel-success">
        <div class="panel-heading ">
            <label>Detaylar</label>

        </div>
        <div class="panel-body">
            <div class="col-sm-3">
                <label for="net_area">Net Alan:</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="number" class="form-control" name="net_area" @if($edit) value="{{ $listing->farm->net_area }}" @endif/>
                        <span class="input-group-addon">m<sup>2</sup></span>
                    </div>

                </div>
            </div>
            <div class="col-sm-3">
                <label for="closed_area">Kapalı Alan:</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="number" class="form-control" name="closed_area" @if($edit) value="{{ $listing->farm->closed_area }}"@endif/>
                        <span class="input-group-addon">m<sup>2</sup></span>
                    </div>

                </div>
            </div>

            <div class="col-sm-6">
                <label>Ada / Parsel :</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="number" class="form-control" name="block" @if($edit)value="{{ $listing->farm->block }}"@endif/>
                        <span class="input-group-addon">Ada</span>
                        <input type="number" class="form-control" name="plot" @if($edit)value="{{ $listing->farm->plot }}"@endif/>
                        <span class="input-group-addon">Parsel</span>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="farm_type">Çiftlik Tipi :</label>
                <div class="form-group">
                    <select class="form-control" name="farm_type" id="farm_type">
                        @foreach($farm_types as $farm_type)
                            <option value="{{$farm_type->id}}" @if($edit && $farm_type->id == $listing->farm->farm_type->id) selected @endif>{{ $farm_type->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="farm_subject">Besi / Süt :</label>
                <div class="form-group">
                    <select class="form-control" name="farm_subject" id="farm_subject">
                        @foreach($farm_subjects as $farm_subject)
                            <option value="{{$farm_subject->id}}"  @if($edit && $farm_subject->id == $listing->farm->farm_subject->id) selected @endif>{{ $farm_subject->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="animal_count">Hayvan Sayısı:</label>
                <div class="form-group">
                    <input type="number" class="form-control" name="animal_count" @if($edit)value="{{ $listing->farm->animal_count }}" @endif/>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-check">
                    <label for="is_active">Üretim Durumu :</label>
                    <input type="checkbox" class="form-check-input"
                           name="is_active" id="is_active"
                           value="1"
                           data-toggle="toggle"
                           data-on="Aktif" data-off="Aktif Değil"
                           data-onstyle="success" data-offstyle="danger" data-size="mini"@if( $edit && $listing->farm->is_active) checked @endif>
                </div>
            </div>

            <div class="col-sm-12">
                <label for="details">Açıklama :</label>
                <div class="form-group">
                       <textarea type="text" class="form-control"
                                 name="details">@if($edit){{ $listing->details }} @endif
                       </textarea>
                </div>
            </div>

        </div>

    </div>
</div>