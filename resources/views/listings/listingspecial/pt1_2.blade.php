<div class="col-sm-12">
    <div class="panel panel-success">
        <div class="panel-heading ">
            <label>Detaylar</label>

        </div>
        <div class="panel-body">
            <div class="col-sm-3">
                <label for="gross_area">Brüt Alan:</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="gross_area" @if($edit) value="{{ $listing->land->gross_area }}" @endif/>
                        <span class="input-group-addon">m<sup>2</sup></span>
                    </div>

                </div>
            </div>
            <div class="col-sm-3">
                <label for="net_area">Net Alan:</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="net_area" @if($edit) value="{{ $listing->land->net_area }}" @endif/>
                        <span class="input-group-addon">m<sup>2</sup></span>
                    </div>

                </div>
            </div>

            <div class="col-sm-6">
                <label>Ada / Parsel :</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="block" @if($edit) value="{{ $listing->land->block }}" @endif/>
                        <span class="input-group-addon">Ada</span>
                        <input type="text" class="form-control" name="plot" @if($edit) value="{{ $listing->land->plot }}" @endif/>
                        <span class="input-group-addon">Parsel</span>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <label for="zoning">İmar Durumu :</label>
                <div class="form-group">
                        <select class="form-control" name="zoning" id="zoning">
                            @foreach($zonings as $zoning)
                                <option value="{{$zoning->id}}" @if($edit && $zoning->id == $listing->land->zoning->id) selected @endif>{{ $zoning->name }}</option>
                            @endforeach
                        </select>
                </div>
            </div>

            <div class="col-sm-12">
                <label for="details">Açıklama :</label>
                <div class="form-group">
                       <textarea type="text" class="form-control"
                                 name="details">@if($edit) {{ $listing->details }} @endif</textarea>
                </div>
            </div>

        </div>

    </div>
</div>