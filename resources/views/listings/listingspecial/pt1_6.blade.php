<div class="col-sm-12">
    <div class="panel panel-success">
        <div class="panel-heading ">
            <label>Detaylar</label>

        </div>
        <div class="panel-body">
            <div class="col-sm-3">
                <label for="gross_area">Brüt Alan:</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="gross_area"
                               @if($edit) value="{{$listing->project->gross_area }}" @endif />
                        <span class="input-group-addon">m<sup>2</sup></span>
                    </div>

                </div>
            </div>
            <div class="col-sm-3">
                <label for="net_area">Net Alan:</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="net_area"
                               @if($edit) value="{{$listing->project->net_area }}" @endif />
                        <span class="input-group-addon">m<sup>2</sup></span>
                    </div>

                </div>
            </div>

            <div class="col-sm-6">
                <label>Oda Sayısı</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="room"
                               @if($edit) value="{{$listing->project->room }}" @endif />
                        <span class="input-group-addon">Oda</span>
                        <input type="text" class="form-control" name="saloon"
                               @if($edit) value="{{$listing->project->saloon }}" @endif />
                        <span class="input-group-addon">Salon</span>
                        <input type="text" class="form-control" name="bath"
                               @if($edit) value="{{$listing->project->bath }}" @endif />
                        <span class="input-group-addon">Banyo</span>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="building_age">Bina Yaşı</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="building_age"
                               @if($edit) value="{{$listing->project->building_age }}" @endif />
                        <span class="input-group-addon">Yıl</span>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="floor_count">Kat Sayısı</label>
                <div class="form-group">
                    <input type="text" class="form-control"
                           id="floor_count" name="floor_count"
                           @if($edit) value="{{$listing->project->floor_count }}" @endif />
                </div>
            </div>

            <div class="col-sm-3">
                <label for="heating_type">Isınma Tipi</label>
                <div class="form-group">
                    <select class="form-control" name="heating_type" id="heating_type">
                        <option value="0">Seçiniz</option>
                        @foreach($heating_types as $heating_type)
                            <option value="{{$heating_type->id}}"
                                    @if($edit && $heating_type->id == $listing->project->heating_type->id) selected @endif >
                                {{ $heating_type->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="col-sm-3">
                <label for="deed_type">Tapu Durumu</label>
                <div class="form-group">
                    <select type="text" class="form-control" name="deed_type">
                        @foreach($deed_types as $deed_type)
                            <option value="{{ $deed_type->id }}"
                                    @if($edit && $deed_type->id == $listing->project->deed_type->id) selected @endif >
                                {{ $deed_type->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="col-sm-3">
                <label for="balcony">Balkon / Teras</label>
                <div class="form-group">
                    <select type="text" class="form-control" name="balcony">
                        <option value="0" @if($edit && $listing->project->balcony == 0) selected @endif>{{ __('Yok') }}</option>
                        <option value="1" @if($edit && $listing->project->balcony == 1) selected @endif>{{ __('Var') }}</option>
                    </select>
                </div>
            </div>


            <div class="col-sm-3">
                <label for="furnished">Eşya Durumu</label>
                <div class="form-group">
                    <select type="text" class="form-control" name="furnished">
                        <option value="0" @if($edit && $listing->project->furnished == 0) selected @endif>{{ __('Yok') }}</option>
                        <option value="1" @if($edit && $listing->project->furnished == 1) selected @endif>{{ __('Var') }}</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="swimming_pool">Yüzme Havuzu</label>
                <div class="form-group">
                    <select type="text" class="form-control" name="swimming_pool">
                        <option value="0"  @if($edit && $listing->project->swimming_pool == 0) selected @endif >{{ __('Yok') }}</option>
                        <option value="1"  @if($edit && $listing->project->swimming_pool == 1) selected @endif >{{ __('Açık') }}</option>
                        <option value="2"  @if($edit && $listing->project->swimming_pool == 2) selected @endif >{{ __('Kapalı') }}</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="security">Güvenlik</label>
                <div class="form-group">
                    <select type="text" class="form-control" name="security">
                        <option value="0" @if($edit && $listing->project->security == 0) selected @endif>{{ __('Yok') }}</option>
                        <option value="1" @if($edit && $listing->project->security == 1) selected @endif>{{ __('Var') }}</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="parking">Otopark</label>
                <div class="form-group">
                    <select type="text" class="form-control" name="parking">
                        <option value="0" @if($edit && $listing->project->parking == 0) selected @endif>{{ __('Yok') }}</option>
                        <option value="1" @if($edit && $listing->project->parking == 1) selected @endif>{{ __('Açık Otopark') }}</option>
                        <option value="2" @if($edit && $listing->project->parking == 2) selected @endif>{{ __('Kapalı Otopark') }}</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="walkingpath">Yürüyüş Parkuru</label>
                <div class="form-group">
                    <select type="text" class="form-control" name="walkingpath">
                        <option value="0" @if($edit && $listing->project->walkingpath == 0) selected @endif>{{ __('Yok') }}</option>
                        <option value="1" @if($edit && $listing->project->walkingpath == 1) selected @endif>{{ __('Var') }}</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="sportsarea">Spor Alanı</label>
                <div class="form-group">
                    <select type="text" class="form-control" name="sportsarea">
                        <option value="0" @if($edit && $listing->project->sportsarea == 0) selected @endif>{{ __('Yok') }}</option>
                        <option value="1" @if($edit && $listing->project->sportsarea == 1) selected @endif>{{ __('Var') }}</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-3">
                <label for="playground">Oyun Alanı</label>
                <div class="form-group">
                    <select type="text" class="form-control" name="playground">
                        <option value="0" @if($edit && $listing->project->playground == 0) selected @endif>{{ __('Yok') }}</option>
                        <option value="1" @if($edit && $listing->project->playground == 1) selected @endif>{{ __('Var') }}</option>
                    </select>
                </div>
            </div>

            {{--
                        <div class="col-sm-3">
                            <div class="form-check-inline">

                                <label for="balcony">Balkon</label>
                                <input type="checkbox" class="form-check-input"
                                       name="balcony" id="balcony"
                                       value="1"
                                       data-toggle="toggle"
                                       data-on="Var" data-off="Yok"
                                       data-onstyle="success" data-offstyle="danger" data-size="mini"
                                       @if($edit && $listing->project->balcony) checked @endif>

                                <label for="furnished">Eşyalı</label>
                                <input type="checkbox" class="form-check-input" name="furnished" id="furnished"
                                       data-toggle="toggle"
                                       data-on="Evet" data-off="Hayır"
                                       data-onstyle="success" data-offstyle="danger" data-size="mini"
                                       @if($edit && $listing->project->furnished) checked @endif>

                            </div>
                        </div>

            --}}

            <div class="col-sm-12">
                <label for="details">Açıklama</label>
                <div class="form-group">
                    <textarea type="text" class="form-control" name="details"></textarea>
                </div>
            </div>

        </div>

    </div>
</div>
