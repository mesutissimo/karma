<div class="modal fade" id="modal_delete_presentation" tabindex="-1" role="dialog" aria-labelledby="mdp_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mdp_label">Sunum Silme</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Kapat">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Bu ilandaki sunumu silmek istediğinize emin misiniz ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">İptal Et</button>
                <button type="button" class="btn btn-danger"  onclick="delete_presentation()"><i class="fas fa-trash"></i> Sil</button>
            </div>
        </div>
    </div>
</div>