<div class="row">

    <div id="imgDropzone"
         class="thumbnail text-center"
         style="">
        <i class="fas fa-5x fa-image"></i><br>Resimleri sürükleyin ya da tıklayıp yükleyin.
    </div>
    <div class="panel panel-warning">
        <div class="panel-heading">Resimler</div>
        <div id="preview" class="panel-body">

        </div>
    </div>

</div>


<div class="hidden" id="tpl">
    <div align="center">
        <div class="col-sm-6 dz-preview dz-file-preview thumbnail" style="margin-bottom:5px;">
                <img class="img-responsive" data-dz-thumbnail/>
                <div class="btn-group" style="position: absolute; bottom: 10px; right: 10px">
                    {{-- <button id="main_image" class="btn btn-xs btn-success">
                        <i class="fas fa-home"></i>
                    </button> --}}
                    <button id="remove_image" class="btn btn-xs btn-danger dz-remove" data-dz-remove>
                        <i class="fas fa-trash"></i>
                    </button>
                </div>

            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span>
            </div>
            <div class="dz-error-message"><span data-dz-errormessage></span></div>
        </div>


    </div>

</div>
