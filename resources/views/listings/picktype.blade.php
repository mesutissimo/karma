<div class="modal fade" id="picktype" tabindex="-1" role="dialog" aria-labelledby="picktypeTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ route('precreate') }}">

                {{ csrf_field() }}

                <div class="modal-header">
                    <h3 class="modal-title" id="picktypeTitle">
                        Yeni Gayrimenkul İlanı
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="listing_type" value="1">
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="form-group">
                                <label for="property_type">Gayrimenkul Tipi :</label>
                                <select name="property_type" id="property_type" class="form-control">
                                    @foreach($property_types as $property_type)
                                        <option value="{{ $property_type->id }}">{{ __($property_type->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">İptal</button>
                    <button type="submit" class="btn btn-primary">Devam Et</button>
                </div>

            </form>
        </div>
    </div>
</div>

<script>

</script>