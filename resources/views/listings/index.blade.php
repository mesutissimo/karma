@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>İlanlar</h3>
                        <h3 class="pull-right">

                        {{--
                            <a href="{{ route('listings.create') }}" class="btn btn-xs btn-primary">
                                <i class="fas fa-plus"></i> İlan Ekle
                            </a>
                        --}}
                            <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#picktype">
                                <i class="fas fa-plus"></i> İlan Ekle
                            </button>

                        </h3>
                    </div>

                    <div class="panel-body">
                        @if(count($listings) > 0)
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th width="150em">Vitrin Fotoğrafı</th>
                                <th>İlan Başlığı</th>
                                <th>İlan Tipi</th>
                                <th>Gayrimenkul Tipi</th>
                                <th>Yer</th>
                                <th>Fiyat</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($listings as $listing)
                                <tr>
                                    <td>
                                        <input type="checkbox" value="{{ $listing->id }}">
                                    </td>
                                    <td>
                                        @if(isset($listing->images[0]))
                                            <img class="img img-responsive"
                                                 src="images/{{ $listing->images[0]->filename }}" alt="">
                                        @else
                                            <img class="img img-responsive" src="images/default/dummy_house.png" alt="">
                                        @endif
                                    </td>
                                    <td>
                                        {{ $listing->title }}
                                    </td>
                                    <td>
                                        {{ $listing->listing_type->name }}
                                    </td>
                                    <td>
                                        {{ $listing->property_type->name }}
                                    </td>
                                    <td>

                                        {{ $listing->province->name }} - {{ $listing->district->name }}

                                    </td>
                                    <td>
                                        {{ $listing->price  }} {{ $listing->currency->symbol }}
                                    </td>
                                    <td>
                                        <form action="{{ route('listings.destroy', $listing->id) }}" method="POST">

                                            <div class="btn-group">
                                                <a href="{{ route('listings.edit',$listing->id)}}"
                                                   class="btn btn-xs btn-primary">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-xs btn-danger">
                                                    <i class="far fa-trash-alt "></i>
                                                </button>
                                            </div>

                                        </form>

                                    </td>

                                </tr>

                            @endforeach

                            </tbody>
                        </table>
@else
                        <p>İlan mevcut değil</p>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('listings.picktype') <!-- Listing type selection modal -->
@endsection

