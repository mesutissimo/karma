@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Ayarlar</div>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#fp_settings" role="tab" aria-controls="home" aria-selected="true">Ön Sayfa Ayarları</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                        </li>
                    </ul>

                    <div class="panel-body">


                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="fp_settings" role="tabpanel" aria-labelledby="home-tab">
                                <div class="col-sm-4">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">Kayan Resimler</div>
                                        <div class="panel-body">
                                            @include('listings.images')
                                        </div>
                                        <div class="panel-footer">
                                            <button id="save_iamges" class="btn btn-xs btn-primary">Kaydet</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
    <script>
        let removedFiles = [];
        Dropzone.autoDiscover = false;

        let idrop = new Dropzone("#imgDropzone", {
            url: "{{ route('image_upload') }}",
            maxFilesize: "5",
            //addRemoveLinks: true,
            previewsContainer: '#preview',
            previewTemplate: document
                .querySelector('#tpl')
                .innerHTML,
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 5,

            init: function() {
                myDropzone = this;
                $.ajax({
                    url: '{{ route('get_images') }}',
                    type: 'post',
                    data: {
                        'target' : 'fp_slider',
                    },
                    dataType: 'json',
                    success: function(response){

                        $.each(response, function(key,value) {
                            let mockFile = { name: value.filename};
                            console.log(myDropzone.file);

                            myDropzone.emit("addedfile", mockFile);
                            myDropzone.emit("thumbnail", mockFile, '/img/slider/'+ value.filename);
                            myDropzone.emit("complete", mockFile);

                        });

                    }
                });
            },


            success: function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('get_images') }}",
                    data: {
                    },

                    contentType: "text/json; charset=utf-8",
                    dataType: "text",
                    success: function (msg) {

                    }
                });
            },


            sending: function (file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
                formData.append("target", "fp_slider");

            },

            completemultiple: function () {
                console.log("Images Uploaded")
                hideLoading();
            }
        });


        idrop.on("addedfile", function (file) {

            file.previewElement.addEventListener("click", (e) => {
                let target = $(e.target);
                console.log(target)
                if (target.is('#remove')) {
                    idrop.removeFile(file);
                }


            })
        });

        idrop.on('removedfile', function (file) {
            removedFiles.push({ 'name' : file.name});
        });

        function deleteRemovedFiles() {

            if(removedFiles.length > 0) {

                let rfJson = JSON.stringify(removedFiles);
                console.log(rfJson)
                $.ajax({
                    type: "POST",
                    url: " {{ route('delete_image') }}",
                    data: {
                        removed_image: rfJson,
                        target: 'fp_slider'
                    },
                    dataType: "json",
                    success: function (msg) {
                        hideLoading();
                    }
                });
                removedFiles = [];


            }

        }

        $('#save_iamges').on('click', function () {
            event.preventDefault();
            showLoading();
            deleteRemovedFiles();
            idrop.processQueue();
            console.log("Images Uploading")
        })

    </script>

@endsection
