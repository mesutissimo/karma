<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IssueStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('issue_statuses')->insert([
            'name' => 'Beklemede',
        ]);

        DB::table('issue_statuses')->insert([
            'name' => 'İnceleniyor',
        ]);

        DB::table('issue_statuses')->insert([
            'name' => 'Cevaplandı',
        ]);

        DB::table('issue_statuses')->insert([
            'name' => 'Çözüldü',
        ]);
    }
}
