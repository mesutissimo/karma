<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HeatingTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('heating_types')->insert([
            'name' => 'Soba'
        ]);
        DB::table('heating_types')->insert([
            'name' => 'Doğalgaz Sobası'
        ]);
        DB::table('heating_types')->insert([
            'name' => 'Kat Kaloriferi'
        ]);
        DB::table('heating_types')->insert([
            'name' => 'Merkezi'
        ]);
        DB::table('heating_types')->insert([
            'name' => 'Merkezi (Payölçer)'
        ]);
        DB::table('heating_types')->insert([
            'name' => 'Doğalgaz (Kombi)'
        ]);
        DB::table('heating_types')->insert([
            'name' => 'Yerden Isıtma'
        ]);
        DB::table('heating_types')->insert([
            'name' => 'Klima'
        ]);
        DB::table('heating_types')->insert([
            'name' => 'FanCoil'
        ]);
        DB::table('heating_types')->insert([
            'name' => 'Güneş Enerjisi'
        ]);
        DB::table('heating_types')->insert([
            'name' => 'Jeotermal'
        ]);
        DB::table('heating_types')->insert([
            'name' => 'VRV'
        ]);
        DB::table('heating_types')->insert([
            'name' => 'Isı Pompası'
        ]);
    }
}
