<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Mesut Uçar',
            'email' => 'garath.edra@gmail.com',
            'password' => bcrypt('1231234'),
        ]);

        DB::table('users')->insert([
            'name' => 'Hatice Kılıç',
            'email' => 'hkilic@karmamuhendislik.net',
            'password' => bcrypt('1231234'),
        ]);

        DB::table('users')->insert([
            'name' => 'Önder Özdemir',
            'email' => 'onder@karmagmg.com',
            'password' => bcrypt('1231234'),
        ]);
    }
}
