<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**₺
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            'name' => 'Türk Lirası',
            'code' => 'TRY',
            'symbol' => '₺',
            'rate' => 1,
        ]);

        DB::table('currencies')->insert([
            'name' => 'Dolar',
            'code' => 'USD',
            'symbol' => '$',
            'rate' => 5,
        ]);

        DB::table('currencies')->insert([
            'name' => 'Avro',
            'code' => 'EUR',
            'symbol' => '€',
            'rate' => 6,
        ]);
    }
}
