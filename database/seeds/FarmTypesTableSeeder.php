<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FarmTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('farm_types')->insert([
            'name' => 'Büyükbaş',
            'symbol' => 'cow'
        ]);
        DB::table('farm_types')->insert([
            'name' => 'Küçükbaş',
            'symbol' => 'sheep'
        ]);
    }
}
