<?php

use Illuminate\Database\Seeder;

class PropertyTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('property_types')->insert([
            'name' => 'Flat',
            'identifier' => 'flat'
        ]);

        DB::table('property_types')->insert([
            'name' => 'Land',
            'identifier' => 'land'
        ]);

        DB::table('property_types')->insert([
            'name' => 'Land In Return For Flat',
            'identifier' => 'land-rff'
        ]);

        DB::table('property_types')->insert([
            'name' => 'Farm',
            'identifier' => 'farm'
        ]);

        DB::table('property_types')->insert([
            'name' => 'Villa',
            'identifier' => 'villa'
        ]);

        DB::table('property_types')->insert([
            'name' => 'Project',
            'identifier' => 'project'
        ]);

    }
}
