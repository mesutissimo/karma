<?php

use Illuminate\Database\Seeder;

class SwimmingPoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('swimming_pools')->insert([
            'name' => 'None',
        ]);

        DB::table('swimming_pools')->insert([
            'name' => 'Outdoor',
        ]);

        DB::table('swimming_pools')->insert([
            'name' => 'Indoor',
        ]);


    }
}
