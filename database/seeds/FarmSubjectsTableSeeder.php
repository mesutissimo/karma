<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FarmSubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('farm_subjects')->insert([
            'name' => 'Besicilik',
            'symbol' => 'barn'
        ]);

        DB::table('farm_subjects')->insert([
            'name' => 'Süt',
            'symbol' => 'milk'

        ]);
    }
}
