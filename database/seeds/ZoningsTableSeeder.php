<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ZoningsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zonings')->insert([
            'name' => 'Konut'
        ]);

        DB::table('zonings')->insert([
            'name' => 'Ticari'
        ]);

        DB::table('zonings')->insert([
            'name' => 'Ticari + Konut'
        ]);

        DB::table('zonings')->insert([
            'name' => 'Toplu Konut'
        ]);

        DB::table('zonings')->insert([
            'name' => 'Turizm'
        ]);

        DB::table('zonings')->insert([
            'name' => 'Villa'
        ]);
    }
}
