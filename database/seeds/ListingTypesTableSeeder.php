<?php

use Illuminate\Database\Seeder;

class ListingTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('listing_types')->insert([
            'name' => 'Satılık'
        ]);

    }
}
