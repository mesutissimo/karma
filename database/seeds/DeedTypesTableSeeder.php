<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeedTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('deed_types')->insert([
            'name' => 'Arsa'
        ]);
        DB::table('deed_types')->insert([
            'name' => 'Kat İrtifakı'
        ]);
        DB::table('deed_types')->insert([
            'name' => 'Kat Mülkiyeti'
        ]);

    }
}
