<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(ListingTypesTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(ListingsTableSeeder::class);
        $this->call(PropertyTypesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DeedTypesTableSeeder::class);
        $this->call(HeatingTypesTableSeeder::class);
        $this->call(ZoningsTableSeeder::class);
        $this->call(FarmTypesTableSeeder::class);
        $this->call(FarmSubjectsTableSeeder::class);
        $this->call(IssueStatusTableSeeder::class);
        $this->call(SwimmingPoolsTableSeeder::class);
    }
}
