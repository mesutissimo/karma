<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlatsTable extends Migration
{

    public function up()
    {
        Schema::create('flats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id');
            $table->integer('gross_area');
            $table->integer('net_area');
            $table->integer('room');
            $table->integer('saloon');
            $table->integer('bath');
            $table->integer('building_age');
            $table->integer('flats_floor');
            $table->integer('building_floor_count');
            $table->tinyInteger('balcony');
            $table->tinyInteger('furnished');
            $table->integer('heating_type_id');
            $table->integer('deed_type_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('flats');
    }
}
