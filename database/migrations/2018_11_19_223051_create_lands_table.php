<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandsTable extends Migration
{
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id');
            $table->tinyInteger('rff');
            $table->integer('gross_area');
            $table->integer('net_area');
            $table->integer('block');
            $table->integer('plot');
            $table->integer('zoning_id');
            $table->integer('total_construction_area')->nullable();
            $table->integer('rff_rate')->nullable();
            $table->integer('apprx_cost')->nullable();
            $table->integer('apprx_gain')->nullable();
            $table->integer('apprx_timetofinish')->nullable();
            $table->integer('kaks')->nullable();
            $table->integer('taks')->nullable();
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('lands');
    }
}
