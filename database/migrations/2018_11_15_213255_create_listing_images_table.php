<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingImagesTable extends Migration
{

    public function up()
    {

        Schema::create('listing_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('listing_uid');
            $table->string('filename');
            $table->string('resized_name');
            $table->string('original_name');
            $table->timestamps();
        });


    }

    public function down()
    {
        Schema::dropIfExists('listing_images');
    }
}
