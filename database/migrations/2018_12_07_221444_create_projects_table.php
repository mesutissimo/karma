<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id');
            $table->integer('gross_area');
            $table->integer('net_area');
            $table->integer('room');
            $table->integer('saloon');
            $table->integer('bath');
            $table->integer('building_age');
            $table->integer('floor_count');
            $table->tinyInteger('balcony');
            $table->tinyInteger('furnished');
            $table->tinyInteger('security');
            $table->tinyInteger('parking');
            $table->tinyInteger('swimming_pool_id');
            $table->tinyInteger('walkingpath');
            $table->tinyInteger('sportsarea');
            $table->tinyInteger('playground');
            $table->integer('heating_type_id');
            $table->integer('deed_type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
