<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{

    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id');
            $table->string('title');
            $table->integer('price');
            $table->integer('currency_id');
            $table->integer('listing_type_id');
            $table->integer('property_type_id');
            $table->integer('province_id');
            $table->integer('district_id');
            $table->integer('precedent_value')->nullable();
            $table->longText('details')->nullable();
            $table->string('presentation')->nullable();
            $table->bigInteger('watch_count')->default(0);
            $table->timestamps();


        });
    }

    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
