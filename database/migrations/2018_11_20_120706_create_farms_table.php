<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFarmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id');
            $table->integer('net_area');
            $table->integer('closed_area');
            $table->integer('block');
            $table->integer('plot');
            $table->integer('farm_type_id');
            $table->tinyInteger('farm_subject_id');
            $table->tinyInteger('is_active')->default(0);
            $table->integer('animal_count');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('farms');
    }
}
