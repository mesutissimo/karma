<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/set-language/{lang}', 'LanguagesController@set')->name('set.language');
Route::get('/', 'HomeController@index')->name('home');
// Route::get('/estates/{property_type}', 'EstatesController@index')->name('estates');


Route::post('/search', 'SearchController@index')->name('searchwith');
Route::get('/search', 'SearchController@index')->name('search');
Route::get('/flats', 'FlatController@index')->name('flats');
Route::get('/villas', 'VillaController@index')->name('villas');
Route::get('/lands', 'LandController@index')->name('lands');
Route::get('/landsrff', 'LandRffController@index')->name('landsrff');
Route::get('/farms', 'FarmController@index')->name('farms');
Route::get('/projects', 'ProjectController@index')->name('projects');
Route::get('/details/{listing_uid}', 'DetailsController@index')->name('details');



Route::get('provinces/get/', 'UtilityController@getProvinces');
Route::get('districts/get/{id}', 'UtilityController@getDistricts');

Auth::routes();

Route::resource('/techsupport', 'TechSupportController');
Route::get('/adminhome', 'AdminHomeController@index')->name('adminhome');
Route::get('/settings', 'SettingsController@index')->name('settings');
Route::resource('/listings', 'ListingsController');
Route::post('/precreate', 'ListingsController@create')->name('precreate');

Route::get('/downloadpresentation/{uid}', 'ListingsController@download_presentation')->name('download_presentation');
Route::get('/deletepresentation/{uid}', 'ListingsController@delete_presentation')->name('delete_presentation');

Route::post('/imgupload', 'ImageController@upload')->name('image_upload');
Route::post('/getimages', 'ImageController@get_images')->name('get_images');
Route::post('/deleteimage', 'ImageController@delete_image')->name('delete_image');







